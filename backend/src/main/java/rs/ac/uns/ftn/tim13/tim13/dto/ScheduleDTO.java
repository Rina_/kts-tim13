package rs.ac.uns.ftn.tim13.tim13.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import rs.ac.uns.ftn.tim13.tim13.model.DayType;


public class ScheduleDTO implements Serializable {

    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy")
    private Date dateFrom;
    private DayType dayType;
    private List<String> times;

    public ScheduleDTO() {
        this.times = new ArrayList<>();
    }

    public ScheduleDTO(Date dateFrom, DayType dayType, List<String> times) {
        this.dateFrom = dateFrom;
        this.dayType = dayType;
        this.times = times;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public DayType getDayType() {
        return dayType;
    }

    public void setDayType(DayType dayType) {
        this.dayType = dayType;
    }

    public List<String> getTimes() {
        return times;
    }

    public void setTimes(List<String> times) {
        this.times = times;
    }
}
