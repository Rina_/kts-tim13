package rs.ac.uns.ftn.tim13.tim13.model;

enum UserType {ADMINISTRATOR, CONTROLLER, PASSENGER}