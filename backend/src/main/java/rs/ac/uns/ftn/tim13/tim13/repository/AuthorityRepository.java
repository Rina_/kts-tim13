package rs.ac.uns.ftn.tim13.tim13.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.ac.uns.ftn.tim13.tim13.model.Authority;

public interface AuthorityRepository extends JpaRepository<Authority, Long> {

    Authority getByName(String name);

}
