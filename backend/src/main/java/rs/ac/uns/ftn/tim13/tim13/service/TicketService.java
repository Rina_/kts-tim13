package rs.ac.uns.ftn.tim13.tim13.service;

import rs.ac.uns.ftn.tim13.tim13.dto.TicketDTO;
import rs.ac.uns.ftn.tim13.tim13.exceptions.DoesNotExistException;
import rs.ac.uns.ftn.tim13.tim13.model.Ticket;

import java.util.ArrayList;
import java.util.Optional;

public interface TicketService {

        public boolean create(TicketDTO ticketDTO) throws DoesNotExistException;
        public ArrayList<Ticket> getUserTickets(String username) throws DoesNotExistException;
        public Ticket getUserTicket(String username, String transportType) throws DoesNotExistException;

}
