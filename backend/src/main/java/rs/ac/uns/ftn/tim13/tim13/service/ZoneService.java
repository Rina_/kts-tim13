package rs.ac.uns.ftn.tim13.tim13.service;

import rs.ac.uns.ftn.tim13.tim13.model.Zone;

public interface ZoneService {

    Zone save(Zone zone);
    Zone findByName(String name);
}
