package rs.ac.uns.ftn.tim13.tim13.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class RegisterPage {

    private WebDriver driver;

    @FindBy(xpath = "//*[@id=\"firstName\"]")
    private WebElement firstName;

    @FindBy(xpath = "//*[@id=\"lastName\"]")
    private WebElement lastName;

    @FindBy(xpath = "//*[@id=\"username\"]")
    private WebElement username;

    @FindBy(xpath = "//*[@id=\"password\"]")
    private WebElement password;

    @FindBy(xpath = "//*[@id=\"confirmPassword\"]")
    private WebElement confirmPassword;

    @FindBy(xpath = "//*[@id=\"email\"]")
    private WebElement email;

    @FindBy(xpath = "//*[@id=\"address\"]")
    private WebElement address;

    @FindBy(xpath = "//*[@id=\"passengerType\"]")
    private WebElement passengerType;

    @FindBy(xpath = "//*[@id=\"passengerType\"]/option[1]")
    private WebElement pensioner;

    @FindBy(xpath = "//*[@id=\"passengerType\"]/option[2]")
    private WebElement student;

    @FindBy(xpath = "//*[@id=\"passengerType\"]/option[3]")
    private WebElement unemployed;

    @FindBy(xpath = "//*[@id=\"passengerType\"]/option[4]")
    private WebElement employed;

    @FindBy(xpath = "//*[@id=\"confirmation\"]")
    private WebElement confirmation;

    @FindBy(xpath = "/html/body/app-root/app-registration/div[2]/div/div/div/div[2]/form/button")
    private WebElement register;

    @FindBy(xpath = "/html/body/app-root/app-registration/div[2]/div/div/div/div[2]/div/a")
    private WebElement login;

    @FindBy(xpath = "//*[@id=\"firstNameRequiredError\"]")
    private WebElement firstNameRequired;

    @FindBy(xpath = "//*[@id=\"lastNameRequiredError\"]")
    private WebElement lastNameRequired;

    @FindBy(xpath = "//*[@id=\"usernameRequiredError\"]")
    private WebElement usernameRequired;

    @FindBy(xpath = "//*[@id=\"passwordRequiredError\"]")
    private WebElement passwordRequired;

    @FindBy(xpath = "//*[@id=\"confirmPasswordRequiredError\"]")
    private WebElement passwordConfirmationRequired;

    @FindBy(xpath = "///*[@id=\"emailRequiredError\"]")
    private WebElement emailRequired;

    @FindBy(xpath = "//*[@id=\"addressRequiredError\"]")
    private WebElement addressRequired;

    @FindBy(xpath = "//*[@id=\"passwordDontMatchError\"]")
    private WebElement passwordsDontMatch;

    @FindBy(xpath = "/html/body")
    private WebElement background;

    @FindBy(xpath = "//*[@id=\"emailValidError\"]")
    private WebElement invalidEmail;

    @FindBy(xpath = "//*[@id=\"toast-container\"]/div")
    private WebElement usernameExists;

    public RegisterPage(WebDriver driver) {
        this.driver = driver;
    }

    public WebElement getUsernameExists() {
        return usernameExists;
    }

    public WebElement getBackground() {
        return background;
    }

    public WebElement getInvalidEmail() {
        return invalidEmail;
    }

    public WebElement getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        WebElement el = getFirstName();
        el.clear();
        el.sendKeys(firstName);
    }

    public WebElement getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        WebElement el = getLastName();
        el.clear();
        el.sendKeys(lastName);
    }

    public WebElement getUsername() {
        return username;
    }

    public void setUsername(String username) {
        WebElement el = getUsername();
        el.clear();
        el.sendKeys(username);
    }

    public WebElement getPassword() {
        return password;
    }

    public void setPassword(String password) {
        WebElement el = getPassword();
        el.clear();
        el.sendKeys(password);
    }

    public WebElement getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        WebElement el = getConfirmPassword();
        el.clear();
        el.sendKeys(confirmPassword);
    }

    public WebElement getEmail() {
        return email;
    }

    public void setEmail(String email) {
        WebElement el = getEmail();
        el.clear();
        el.sendKeys(email);
    }

    public WebElement getAddress() {
        return address;
    }

    public void setAddress(String address) {
        WebElement el = getAddress();
        el.clear();
        el.sendKeys(address);
    }

    public WebElement getPassengerType() {
        return passengerType;
    }

    public void setPassengerType(WebElement passengerType) {
        this.passengerType = passengerType;
    }

    public WebElement getPensioner() {
        return pensioner;
    }

    public void setPensioner(WebElement pensioner) {
        this.pensioner = pensioner;
    }

    public WebElement getStudent() {
        return student;
    }

    public void setStudent(WebElement student) {
        this.student = student;
    }

    public WebElement getUnemployed() {
        return unemployed;
    }

    public void setUnemployed(WebElement unemployed) {
        this.unemployed = unemployed;
    }

    public WebElement getEmployed() {
        return employed;
    }

    public void setEmployed(WebElement employed) {
        this.employed = employed;
    }

    public WebElement getConfirmation() {
        return confirmation;
    }

    public void setConfirmation(String confirmation) {
        WebElement el = getConfirmation();
        el.clear();
        el.sendKeys(confirmation);
    }

    public WebElement getRegister() {
        return register;
    }

    public void setRegister(WebElement register) {
        this.register = register;
    }

    public WebElement getLogin() {
        return login;
    }

    public void setLogin(WebElement login) {
        this.login = login;
    }

    public WebElement getFirstNameRequired() {
        return firstNameRequired;
    }

    public WebElement getLastNameRequired() {
        return lastNameRequired;
    }

    public WebElement getUsernameRequired() {
        return usernameRequired;
    }

    public WebElement getPasswordRequired() {
        return passwordRequired;
    }

    public WebElement getPasswordConfirmationRequired() {
        return passwordConfirmationRequired;
    }

    public WebElement getEmailRequired() {
        return emailRequired;
    }

    public WebElement getAddressRequired() {
        return addressRequired;
    }

    public WebElement getPasswordsDontMatch() {
        return passwordsDontMatch;
    }


    public void ensureIsDisplayedFirstName(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(firstName));
    }

    public void ensureIsDisplayedLastName(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(lastName));
    }

    public void ensureIsDisplayedUsername(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(username));
    }

    public void ensureIsDisplayedPassword(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(password));
    }

    public void ensureIsDisplayedPasswordConfirmation(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(confirmPassword));
    }

    public void ensureIsDisplayedEmail(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(email));
    }

    public void ensureIsDisplayedAddress(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(address));
    }

    public void ensureIsDisplayedPassengerType(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(passengerType));
    }

    public void ensureIsDisplayedConfirmation(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(confirmation));
    }

    public void ensureIsDisplayedRegister(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(register));
    }

    public void ensureIsDisplayedLogin(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(login));
    }

    public void ensureIsDisplayedPensioner(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(pensioner));
    }

    public void ensureIsDisplayedStudent(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(student));
    }

    public void ensureIsDisplayedEmployed(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(employed));
    }

    public void ensureIsDisplayedUnemployed(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(unemployed));
    }

    public void ensureIsDisplayedUsernameRequired(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(usernameRequired));
    }

    public void ensureIsDisplayedFirstNameRequired(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(firstNameRequired));
    }

    public void ensureIsDisplayedLastNameRequired(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(lastNameRequired));
    }

    public void ensureIsDisplayedPasswordRequired(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(passwordRequired));
    }

    public void ensureIsDisplayedPasswordConfirmationRequired(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(passwordConfirmationRequired));
    }

    public void ensureIsDisplayedEmailRequired(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(emailRequired));
    }

    public void ensureIsDisplayedAddressRequired(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(addressRequired));
    }

    public void ensureIsDisplayedPasswordsDontMatch(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(passwordsDontMatch));
    }

    public void ensureIsDisplayedIvalidEmail(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(invalidEmail));
    }

    public void ensureIsDisplayedUsernameExists(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(usernameExists));
    }
}
