package rs.ac.uns.ftn.tim13.tim13.service;

import org.springframework.scheduling.annotation.Async;

import javax.mail.MessagingException;

public interface EmailService {

    @Async
    void sendMail(String title, String text, String to) throws MessagingException;

}
