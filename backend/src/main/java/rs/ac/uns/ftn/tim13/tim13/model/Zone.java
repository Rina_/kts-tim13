package rs.ac.uns.ftn.tim13.tim13.model;


import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "zone")
public class Zone {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    @Column(nullable = false)
    private String name;

    @OneToMany(mappedBy = "zone", fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private List<Line> lines;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private List<Ticket> tickets;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private List<PriceTicket> priceTickets;

    public Zone() {
    }

    public Zone(String name) {

        this.name = name;
    }

    public Long getId() {

        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Line> getLines() {
        return lines;
    }

    public void setLines(List<Line> lines) {
        this.lines = lines;
    }

    public List<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(List<Ticket> tickets) {
        this.tickets = tickets;
    }

    public List<PriceTicket> getPriceTickets() {
        return priceTickets;
    }

    public void setPriceTickets(List<PriceTicket> priceTickets) {
        this.priceTickets = priceTickets;
    }
}
