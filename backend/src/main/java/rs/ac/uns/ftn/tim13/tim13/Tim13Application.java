package rs.ac.uns.ftn.tim13.tim13;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Tim13Application {

	public static void main(String[] args) {
		SpringApplication.run(Tim13Application.class, args);
	}
}
