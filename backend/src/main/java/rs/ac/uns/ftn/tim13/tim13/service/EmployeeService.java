package rs.ac.uns.ftn.tim13.tim13.service;

import rs.ac.uns.ftn.tim13.tim13.dto.EmployeeDTO;
import rs.ac.uns.ftn.tim13.tim13.dto.PassengerDTO;
import rs.ac.uns.ftn.tim13.tim13.exceptions.EmployeeNotFoundException;
import rs.ac.uns.ftn.tim13.tim13.exceptions.PassengerNotFoundException;
import rs.ac.uns.ftn.tim13.tim13.exceptions.UserFromTokenNotFoundException;
import rs.ac.uns.ftn.tim13.tim13.model.Employee;
import rs.ac.uns.ftn.tim13.tim13.model.Passenger;

import java.util.List;

public interface EmployeeService {

    Employee findByUsername(String username);

    Employee save(Employee employee);

    List<Employee> getAll();

    long findEmploymentNumber();

    Employee newEmployee(EmployeeDTO dto);

    List<PassengerDTO> getAllUnverified();

    Passenger verifyPassenger(String token, Long id) throws UserFromTokenNotFoundException, EmployeeNotFoundException, PassengerNotFoundException;

    boolean denyPassenger(Long id) throws PassengerNotFoundException;
}
