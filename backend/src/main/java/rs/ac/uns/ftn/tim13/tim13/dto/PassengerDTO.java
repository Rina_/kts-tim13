package rs.ac.uns.ftn.tim13.tim13.dto;

import rs.ac.uns.ftn.tim13.tim13.model.Passenger;

import java.io.Serializable;

public class PassengerDTO implements Serializable {

    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private String address;
    private String confirmation;
    private String passengerType;
    private String username;
    private String password;

    public PassengerDTO(Long id, String firstName, String lastName, String email, String address, String confirmation, String passengerType, String username, String password) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.address = address;
        this.confirmation = confirmation;
        this.passengerType = passengerType;
        this.username = username;
        this.password = password;
    }

    public PassengerDTO(Passenger p){
        this.id = p.getId();
        this.firstName = p.getFirstName();
        this.lastName = p.getLastName();
        this.email = p.getEmail();
        this.address = p.getAddress();
        this.confirmation = p.getConfirmation();
        this.passengerType = p.getPassengerType().toString();
    }

    public String getUsername() {

        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public PassengerDTO() {
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getConfirmation() {
        return confirmation;
    }

    public void setConfirmation(String confirmation) {
        this.confirmation = confirmation;
    }

    public String getPassengerType() {
        return passengerType;
    }

    public void setPassengerType(String passengerType) {
        this.passengerType = passengerType;



    }


    @Override
    public String toString() {
        return "PassengerDTO{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", address='" + address + '\'' +
                ", confirmation='" + confirmation + '\'' +
                ", passengerType='" + passengerType + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
