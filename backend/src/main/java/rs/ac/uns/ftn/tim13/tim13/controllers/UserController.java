package rs.ac.uns.ftn.tim13.tim13.controllers;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import rs.ac.uns.ftn.tim13.tim13.dto.EmployeeDTO;
import rs.ac.uns.ftn.tim13.tim13.dto.LoginDTO;
import rs.ac.uns.ftn.tim13.tim13.dto.PassengerDTO;
import rs.ac.uns.ftn.tim13.tim13.dto.TokenDTO;
import rs.ac.uns.ftn.tim13.tim13.exceptions.*;
import rs.ac.uns.ftn.tim13.tim13.model.*;
import rs.ac.uns.ftn.tim13.tim13.service.*;

@RestController
@RequestMapping("/api")
public class UserController {
	
	@Autowired
	private AccountService accountService;

    @Autowired
    private PassengerService passengerService;

    @Autowired
	private EmployeeService employeeService;


    @RequestMapping(value = "/register",
					method = RequestMethod.POST,
					consumes = MediaType.APPLICATION_JSON_VALUE,
					produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity register(@RequestBody PassengerDTO dto) throws AuthorityNotFoundException {


		Passenger passenger = passengerService.registerPassenger(dto);

		if(passenger == null)
			return new ResponseEntity(HttpStatus.CONFLICT);


		return new ResponseEntity(HttpStatus.OK);



	}


	@PostMapping(
			value = "/login",
			consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE
	)
	public ResponseEntity login(
			@RequestBody LoginDTO loginDTO) {


    	TokenDTO tokenDTO= accountService.login(loginDTO);

    	if(tokenDTO == null) {

			return new ResponseEntity(HttpStatus.NOT_FOUND);
		}

    	return new ResponseEntity(tokenDTO, HttpStatus.OK);


	}

	@RequestMapping(value = "get_all",
					method = RequestMethod.GET,
					produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity getAll(){


		List<PassengerDTO> passengerDTOS = employeeService.getAllUnverified();

		return new ResponseEntity(passengerDTOS, HttpStatus.OK);

	}


	@RequestMapping(value = "accept_request",
					method = RequestMethod.PATCH,
					produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity acceptRequest(@RequestHeader("Authentication-Token") String token,
										 @RequestParam("id") Long id) throws UserFromTokenNotFoundException, PassengerNotFoundException, EmployeeNotFoundException {

		Passenger passenger = new Passenger();

		try{
			passenger = employeeService.verifyPassenger(token, id);
		}
		catch(Exception ex){
			return new ResponseEntity(HttpStatus.BAD_REQUEST);

		}

		if(passenger == null) return new ResponseEntity(HttpStatus.BAD_REQUEST);

		return new ResponseEntity(HttpStatus.OK);
	}


	@RequestMapping(value = "deny_request",
					method = RequestMethod.GET,
					produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity denyRequest(@RequestParam("id") Long id) throws PassengerNotFoundException {

		boolean bool = employeeService.denyPassenger(id);

		if(bool)
			return new ResponseEntity(HttpStatus.OK);

		return new ResponseEntity(HttpStatus.FAILED_DEPENDENCY);

	}


	@RequestMapping(value = "new_employee",
					method = RequestMethod.POST,
			consumes = MediaType.APPLICATION_JSON_VALUE,
					produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity newEmployee(@RequestBody EmployeeDTO dto){

		Employee employee = this.employeeService.newEmployee(dto);

		if(employee == null)
			return new ResponseEntity(HttpStatus.CONFLICT);

		return new ResponseEntity(HttpStatus.OK);
	}
}
