package rs.ac.uns.ftn.tim13.tim13.service;

import rs.ac.uns.ftn.tim13.tim13.dto.PassengerDTO;
import rs.ac.uns.ftn.tim13.tim13.exceptions.AuthorityNotFoundException;
import rs.ac.uns.ftn.tim13.tim13.exceptions.EmployeeNotFoundException;
import rs.ac.uns.ftn.tim13.tim13.exceptions.PassengerNotFoundException;
import rs.ac.uns.ftn.tim13.tim13.exceptions.UserFromTokenNotFoundException;
import rs.ac.uns.ftn.tim13.tim13.model.Passenger;

import java.util.Optional;
import java.util.List;

public interface PassengerService {

    Passenger save(Passenger passenger);

    List<Passenger> findUnverifiedPassengers();

    Optional<Passenger> findById(Long id);

    void deletePassenger(Passenger passenger);

    Passenger registerPassenger(PassengerDTO dto) throws AuthorityNotFoundException;

}
