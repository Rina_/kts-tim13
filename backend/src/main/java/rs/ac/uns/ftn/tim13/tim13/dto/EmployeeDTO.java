package rs.ac.uns.ftn.tim13.tim13.dto;

import rs.ac.uns.ftn.tim13.tim13.model.Employee;

import java.io.Serializable;
import java.util.Date;

public class EmployeeDTO implements Serializable {

    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private String address;
    private Long phoneNumber;
    private Date employmentDate;
    private Long employmentNumber;
    private String username;
    private String password;
    private String authority;

    public EmployeeDTO() {
    }

    public EmployeeDTO(Long id, String firstName, String lastName,
                       String email, String address, Long phoneNumber,
                       Date employmentDate, Long employmentNumber,
                       String username, String password, String authority) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.employmentDate = employmentDate;
        this.employmentNumber = employmentNumber;
        this.username = username;
        this.password = password;
        this.authority = authority;
    }

    public EmployeeDTO(Employee e) {
        this.firstName = e.getFirstName();
        this.lastName = e.getLastName();
        this.email = e.getEmail();
        this.address = e.getAddress();
        this.phoneNumber = e.getPhoneNumber();
        this.employmentDate = e.getEmploymentDate();
        this.employmentNumber = e.getEmploymentNumber();
        this.username = e.getAccount().getUsername();
        this.password = e.getAccount().getPassword();
        this.authority = e.getAccount().getAuthority().getName();
    }

    public EmployeeDTO(String firstName, String lastName, String email, String address, Long phoneNumber, String username, String password, String authority) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.username = username;
        this.password = password;
        this.authority = authority;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Long getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(Long phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Date getEmploymentDate() {
        return employmentDate;
    }

    public void setEmploymentDate(Date employmentDate) {
        this.employmentDate = employmentDate;
    }

    public Long getEmploymentNumber() {
        return employmentNumber;
    }

    public void setEmploymentNumber(Long employmentNumber) {
        this.employmentNumber = employmentNumber;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }
}
