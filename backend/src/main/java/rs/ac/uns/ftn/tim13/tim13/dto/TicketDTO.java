package rs.ac.uns.ftn.tim13.tim13.dto;


public class TicketDTO {

    private String user;
    private String ticketType;
    private String transportType;

    public TicketDTO(){

    }

    public TicketDTO (String user, String ticketType, String transportType){
        this.user = user;
        this.ticketType = ticketType;
        this.transportType = transportType;

    }


    public String getUser() { return user; }
    public void setUser(String user) {this.user=user;}
    public String getTicketType() { return ticketType;}
    public void setTicketType(String ticketType) {this.ticketType=ticketType;}
    public String getTransportType(){ return transportType; }
    public void setTransportType(String transportType) {this.transportType=transportType;}
}
