package rs.ac.uns.ftn.tim13.tim13.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.tim13.tim13.model.Stop;
import rs.ac.uns.ftn.tim13.tim13.repository.StopRepository;

@Service
public class StopServiceImpl implements StopService{

    @Autowired
    private StopRepository stopRepository;


    @Override
    public Stop findByName(String name) {
        return stopRepository.findByName(name);
    }

    @Override
    public Stop findByXandY(double x, double y) {
        return stopRepository.findByXandY(x,y);
    }

    @Override
    public Stop save(Stop stop) {
        return stopRepository.save(stop);
    }
}
