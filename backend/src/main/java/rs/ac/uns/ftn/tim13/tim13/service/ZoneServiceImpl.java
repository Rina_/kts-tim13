package rs.ac.uns.ftn.tim13.tim13.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.tim13.tim13.model.Zone;
import rs.ac.uns.ftn.tim13.tim13.repository.ZoneRepository;

@Service
public class ZoneServiceImpl implements ZoneService {

    @Autowired
    private ZoneRepository zoneRepository;

    @Override
    public Zone save(Zone zone) {
        return zoneRepository.save(zone);
    }

    @Override
    public Zone findByName(String name) {
        return zoneRepository.findByName(name);
    }
}
