package rs.ac.uns.ftn.tim13.tim13.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.tim13.tim13.dto.PassengerDTO;
import rs.ac.uns.ftn.tim13.tim13.exceptions.*;
import rs.ac.uns.ftn.tim13.tim13.model.*;
import rs.ac.uns.ftn.tim13.tim13.repository.PassengerRepository;
import rs.ac.uns.ftn.tim13.tim13.security.JWTUtils;

import javax.mail.MessagingException;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

@Service
public class PassengerServiceImpl implements PassengerService {

    @Autowired
    PassengerRepository passengerRepository;

    @Autowired
    private AccountService accountService;

    @Autowired
    private EmailService emailService;

    @Autowired
    private AuthorityService authorityService;

    @Override
    public Passenger save(Passenger passenger) {
        return passengerRepository.save(passenger);
    }

    @Override
    public List<Passenger> findUnverifiedPassengers() {
        return passengerRepository.findUnverifiedPassengers();
    }

    @Override
    public Optional<Passenger> findById(Long id) {
        return passengerRepository.findById(id);
    }

    @Override
    public void deletePassenger(Passenger passenger) {
        passengerRepository.delete(passenger);
    }



    @Override
    public Passenger registerPassenger(PassengerDTO dto) throws AuthorityNotFoundException {

        Passenger p = new Passenger();
        p.setAccount(new Account());
        p.setFirstName(dto.getFirstName());
        p.setLastName(dto.getLastName());
        p.setAddress(dto.getAddress());
        p.setConfirmation(dto.getConfirmation());
        p.getAccount().setUsername(dto.getUsername());
        if(dto.getPassengerType().equalsIgnoreCase("student")){
            p.setPassengerType(PassengerType.STUDENT);
        }
        else if(dto.getPassengerType().equalsIgnoreCase("pensioner")){
            p.setPassengerType(PassengerType.PENSIONER);
        }
        else if(dto.getPassengerType().equalsIgnoreCase("employed")){
            p.setPassengerType(PassengerType.EMPLOYED);
        }
        else{
            p.setPassengerType(PassengerType.UNEMPLOYED);
        }
        p.setEmail(dto.getEmail());
        p.getAccount().setPassword(BCrypt.hashpw(dto.getPassword(), BCrypt.gensalt()));

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MONTH, 10);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.add(Calendar.YEAR, 1);

        p.setExpirationDate(calendar.getTime());

        Authority authority = authorityService.getByName("PASSENGER");

        if(authority == null) throw new AuthorityNotFoundException();

        p.getAccount().setAuthority(authority);
        try {
            Account uu = accountService.register(p.getAccount());
            uu.setUserStatus((p.getPassengerType().equals(PassengerType.EMPLOYED)) ? UserStatus.ACTIVATED : UserStatus.PENDING);
            p.setAccount(uu);
            save(p);

            p.getAccount().setPassenger(p);
            accountService.save(p.getAccount());

            authority.getAccounts().add(uu);
            authority = authorityService.save(authority);
            uu.setAuthority(authority);
            accountService.save(uu);

            if(p.getPassengerType() == PassengerType.valueOf("EMPLOYED")) {


                try {
                    emailService.sendMail("Your account has been verified!", "Dear" + p.getFirstName() +
                            "Your account has officially been verified and you can start using it. Best regards", p.getEmail());
                }
                catch (MessagingException m) {
                    m.printStackTrace();
                    return null;
                }
            }

            return p;
        }
        catch (UsernameExistsException e){
            return null;
        }

    }


}
