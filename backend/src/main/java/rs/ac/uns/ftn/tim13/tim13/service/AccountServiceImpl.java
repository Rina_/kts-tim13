package rs.ac.uns.ftn.tim13.tim13.service;

import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.tim13.tim13.dto.LoginDTO;
import rs.ac.uns.ftn.tim13.tim13.dto.TokenDTO;
import rs.ac.uns.ftn.tim13.tim13.exceptions.AccountNotFoundException;
import rs.ac.uns.ftn.tim13.tim13.exceptions.UsernameExistsException;
import rs.ac.uns.ftn.tim13.tim13.model.Account;
import rs.ac.uns.ftn.tim13.tim13.model.PassengerType;
import rs.ac.uns.ftn.tim13.tim13.model.UserStatus;
import rs.ac.uns.ftn.tim13.tim13.repository.AccountRepository;
import rs.ac.uns.ftn.tim13.tim13.security.JWTUtils;


@Service
public class AccountServiceImpl implements AccountService {


	@Autowired
	private AccountRepository accountRepository;

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private UserDetailsServiceImpl userDetailsService;

	@Autowired
	private JWTUtils jwtUtils;
	
	public Account register(Account u) throws UsernameExistsException{
		Account old_account = accountRepository.findByUsername(u.getUsername());

		if (old_account != null) {
			throw new UsernameExistsException();
		}

			u = accountRepository.save(u);
			return u;


	}

	@Override
	public Account findByUsername(String username) {
		return this.accountRepository.findByUsername(username);
	}

	@Override
	public Account save(Account account) {
		return accountRepository.save(account);
	}

	@Override
	public void deleteAccount(Account account) {
		accountRepository.delete(account);
	}

    @Override
    public TokenDTO login(LoginDTO loginDTO) {
		try {

			Account account = findByUsername(loginDTO.getUsername());

			if (account == null) throw new AccountNotFoundException();
			if(account.getAuthority().getName().equals("PASSENGER")) {
				if (account.getPassenger().getVerifiedBy() == null && (account.getPassenger().getPassengerType() != PassengerType.valueOf("EMPLOYED")))
					throw new AccountNotFoundException();
			}UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(
					loginDTO.getUsername(), loginDTO.getPassword());
			authenticationManager.authenticate(token);
			UserDetails details = userDetailsService.loadUserByUsername(account.getUsername());

			TokenDTO userToken = new TokenDTO(jwtUtils.generateToken(details));

			return userToken;
		} catch (AccountNotFoundException ex) {
			ex.printStackTrace();
			return null;
		}
    }
}
