package rs.ac.uns.ftn.tim13.tim13.repository;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.ac.uns.ftn.tim13.tim13.model.Account;


public interface AccountRepository extends JpaRepository<Account, Long>{
	Optional<Account> findById(Long id);
	Account findByUsername(String username);
	Account save(Account u);
}
