package rs.ac.uns.ftn.tim13.tim13.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.tim13.tim13.model.Authority;
import rs.ac.uns.ftn.tim13.tim13.repository.AuthorityRepository;

@Service
public class AuthorityServiceImpl implements AuthorityService{

    @Autowired
    AuthorityRepository authorityRepository;

    @Override
    public Authority getByName(String name) {
        return authorityRepository.getByName(name);
    }

    @Override
    public Authority save(Authority authority) {
        return authorityRepository.save(authority);
    }
}
