package rs.ac.uns.ftn.tim13.tim13.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import rs.ac.uns.ftn.tim13.tim13.dto.LineDTO;
import rs.ac.uns.ftn.tim13.tim13.dto.ScheduleDTO;
import rs.ac.uns.ftn.tim13.tim13.dto.StopDTO;
import rs.ac.uns.ftn.tim13.tim13.model.*;
import rs.ac.uns.ftn.tim13.tim13.service.*;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/api/lines")
public class LineController {


    @Autowired
    private ZoneService zoneService;

    @Autowired
    private StopService stopService;

    @Autowired
    private ScheduleService scheduleService;

    @Autowired
    private LineService lineService;

    @Autowired
    private MyLocalTimeService myLocalTimeService;

    @PostMapping
    (path = "/",
    consumes = MediaType.APPLICATION_JSON_VALUE,
    produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity createLine(@RequestBody LineDTO dto){
        System.out.println(dto);

        if(dto.getLineName() == null) return new ResponseEntity(HttpStatus.BAD_REQUEST);

        Line line = new Line();
        line.setName(dto.getLineName());
        Zone zone = zoneService.findByName(dto.getZoneName());
        if(zone == null)
        {
            zone = new Zone();
            zone.setName(dto.getZoneName());
            zone = zoneService.save(zone);
        }
        line.setZone(zone);
        Stop stop;
        List<Stop> stops = new ArrayList<>();

        for(StopDTO stopDTO: dto.getStops()){
            //ako je ime stajalista popunjeno
            if(stopDTO.getName() != null) {
                stop = stopService.findByName(stopDTO.getName());
                //ako ne postoji stajaliste sa tim imenom
                if(stop == null)
                {
                    stop = new Stop(stopDTO.getName(), stopDTO.getxCoordinate(), stopDTO.getyCoordinate());
                    stop = stopService.save(stop);
                    stops.add(stop);
                }
            }
            else{
                stop = stopService.findByXandY(stopDTO.getxCoordinate(), stopDTO.getyCoordinate());
                //Ako ne postoji stajaliste sa tim koordinatam u bazi, vraca se bad request
                //Jer se ne moze napraviti novo stajalise bez imena
                if(stop == null) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
                stops.add(stop);
            }
        }
        Schedule schedule;
        List<Schedule> schedules = new ArrayList<>();
        for (ScheduleDTO sc:
             dto.getSchedules()) {
            schedule = new Schedule(sc.getDateFrom(), sc.getDayType(), new ArrayList<>());
            for (String str :
                    sc.getTimes()) {
                LocalTime localTime = LocalTime.parse(str);
                MyLocalTime time = new MyLocalTime(localTime);
                time = myLocalTimeService.save(time);
                schedule.getLocalTimes().add(time);
            }

            schedule = scheduleService.save(schedule);
            schedules.add(schedule);
        }

        line.setSchedules(schedules);
        line.setStops(stops);

        line = lineService.save(line);

        for (Schedule sc:
             line.getSchedules()) {
            sc.setLine(line);
            scheduleService.save(sc);
        }

        for(Stop s : line.getStops()){
            List<Line> lines = s.getLines();
            if(lines == null)
                s.setLines(new ArrayList<>());
            s.getLines().add(line);
            stopService.save(s);
        }

        //TODO: videti sta raditi sa transport






        return new ResponseEntity<>(HttpStatus.OK);
    }
}
