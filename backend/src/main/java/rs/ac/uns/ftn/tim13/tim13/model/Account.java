package rs.ac.uns.ftn.tim13.tim13.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "account")
public class Account {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(nullable = false)
	private String username;

	@Column(nullable = false)
	private String password;

	@OneToOne
    @JsonManagedReference
    private Passenger passenger;

	@OneToOne
	@JsonManagedReference
	private Employee employee;

	@Enumerated(EnumType.STRING)
	private UserStatus userStatus;

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	private Authority authority;


	public Account() {
	}

	public Account(String username, String password,
				   UserStatus userStatus) {
		this.username = username;
		this.password = password;
		this.userStatus = userStatus;

	}

	public Account(String username, String password){
		this.username = username;
		this.password = password;
	}

	public Long getId() {

		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}


	public UserStatus getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(UserStatus userStatus) {
		this.userStatus = userStatus;
	}

	public Passenger getPassenger() {
		return passenger;
	}

	public void setPassenger(Passenger passenger) {
		this.passenger = passenger;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Authority getAuthority() {
		return authority;
	}

	public void setAuthority(Authority authority) {
		this.authority = authority;
	}
}
