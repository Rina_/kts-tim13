package rs.ac.uns.ftn.tim13.tim13.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.tim13.tim13.dto.TicketDTO;
import rs.ac.uns.ftn.tim13.tim13.exceptions.DoesNotExistException;
import rs.ac.uns.ftn.tim13.tim13.model.*;
import rs.ac.uns.ftn.tim13.tim13.repository.AccountRepository;
import rs.ac.uns.ftn.tim13.tim13.repository.PriceTicketRepository;
import rs.ac.uns.ftn.tim13.tim13.repository.TicketRepository;

import java.util.ArrayList;

@Service
public class TicketServiceImpl implements TicketService{

    @Autowired
    private TicketRepository ticketRepository;

    @Autowired
    private PriceTicketRepository priceTicketRepository;

    @Autowired
    private AccountRepository accountRepository;

    public boolean create(TicketDTO ticketDTO) throws DoesNotExistException{
        if (ticketDTO==null) throw new DoesNotExistException();
        Account p = accountRepository.findByUsername(ticketDTO.getUser());
        if (p==null) throw new DoesNotExistException();
        TicketType ticktype= TicketType.valueOf(ticketDTO.getTicketType());
        TransportType trantype = TransportType.valueOf(ticketDTO.getTransportType());
        PriceTicket pt =priceTicketRepository.findByTicketType(ticktype);
        if (pt==null) throw new DoesNotExistException();
        Ticket ticket = new Ticket();
        ticket.setPrice(pt.getPrice());
        ticket.setTicketType(ticktype);
        ticket.setPassenger(p.getPassenger());

        ticketRepository.save(ticket);
        return true;
    }

    public ArrayList<Ticket> getUserTickets(String username) throws DoesNotExistException{

        Account p =  accountRepository.findByUsername(username);
        if (p==null) throw new DoesNotExistException();
        ArrayList<Ticket> tickets= ticketRepository.findbyUsername(p.getUsername());
        return tickets;

    }

    public Ticket getUserTicket(String username, String transportType) throws DoesNotExistException{

        ArrayList<Ticket> tickets = getUserTickets(username);
        TransportType type = TransportType.valueOf(transportType);
        for (Ticket t: tickets){
            return t;
        }
        throw new DoesNotExistException();

    }




}
