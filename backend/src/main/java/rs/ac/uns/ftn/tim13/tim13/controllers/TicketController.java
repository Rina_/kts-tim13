package rs.ac.uns.ftn.tim13.tim13.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import rs.ac.uns.ftn.tim13.tim13.dto.TicketDTO;
import rs.ac.uns.ftn.tim13.tim13.exceptions.DoesNotExistException;
import rs.ac.uns.ftn.tim13.tim13.service.TicketServiceImpl;

@RestController
@RequestMapping(value = "api/ticket")
public class TicketController {

    @Autowired
    private TicketServiceImpl ticketService;

    @PreAuthorize("permitAll()")

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ResponseEntity<String> createTicket(@RequestBody TicketDTO ticketDTO){
        try {
            ticketService.create(ticketDTO);

            return new ResponseEntity<String>("Ticket created.", HttpStatus.OK);
        }
        catch(DoesNotExistException dne){
            return new ResponseEntity<String>("Ticket not created."+ dne.getMessage(), HttpStatus.BAD_REQUEST);
        }

    }

}
