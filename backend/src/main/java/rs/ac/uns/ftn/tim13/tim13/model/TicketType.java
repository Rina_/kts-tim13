package rs.ac.uns.ftn.tim13.tim13.model;

public enum TicketType {ONETIME, DAYLY, MONTHLY, YEARLY}