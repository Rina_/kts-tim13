package rs.ac.uns.ftn.tim13.tim13.service;
import org.apache.xml.serializer.utils.SerializerMessages_zh;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.tim13.tim13.dto.EmployeeDTO;
import rs.ac.uns.ftn.tim13.tim13.dto.PassengerDTO;
import rs.ac.uns.ftn.tim13.tim13.exceptions.EmployeeNotFoundException;
import rs.ac.uns.ftn.tim13.tim13.exceptions.PassengerNotFoundException;
import rs.ac.uns.ftn.tim13.tim13.exceptions.UserFromTokenNotFoundException;
import rs.ac.uns.ftn.tim13.tim13.model.Account;
import rs.ac.uns.ftn.tim13.tim13.model.Employee;
import rs.ac.uns.ftn.tim13.tim13.model.Passenger;
import rs.ac.uns.ftn.tim13.tim13.model.UserStatus;
import rs.ac.uns.ftn.tim13.tim13.repository.EmployeeRepository;
import rs.ac.uns.ftn.tim13.tim13.security.JWTUtils;

import javax.mail.MessagingException;
import java.util.*;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private AuthorityService authorityService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private PassengerService passengerService;

    @Autowired
    private EmailService emailService;


    @Autowired
    private JWTUtils jwtUtils;

    @Override
    public Employee findByUsername(String username) {
        return employeeRepository.findByUsername(username);
    }

    @Override
    public Employee save(Employee employee) {
        return employeeRepository.save(employee);
    }

    @Override
    public List<Employee> getAll() {
        return employeeRepository.findAll();
    }

    @Override
    public long findEmploymentNumber() {
        return employeeRepository.findEmploymentNumber();
    }

    @Override
    public Employee newEmployee(EmployeeDTO dto) {

        Account account = accountService.findByUsername(dto.getUsername());

        if(account != null) return null;

        Employee employee = new Employee();
        employee.setFirstName(dto.getFirstName());
        employee.setLastName(dto.getLastName());
        employee.setPhoneNumber(dto.getPhoneNumber());
        employee.setAddress(dto.getAddress());

        long employmentNumber = findEmploymentNumber();
        Random rand = new Random();
        employee.setEmploymentNumber(employmentNumber + (rand.nextInt(10)+1));

        employee.setEmploymentDate(new Date());
        employee.setEmail(dto.getEmail());
        Account a = new Account(dto.getUsername(), BCrypt.hashpw(dto.getPassword(), BCrypt.gensalt()), UserStatus.ACTIVATED);

        employee = save(employee);


        a.setAuthority(authorityService.getByName(dto.getAuthority()));

        a = accountService.save(a);

        a.setEmployee(employee);
        employee.setAccount(a);
        save(employee);
        accountService.save(a);


        return employee;


    }

    @Override
    public List<PassengerDTO> getAllUnverified() {
        List<Passenger> passengers = passengerService.findUnverifiedPassengers();

        List<PassengerDTO> passengerDTOS = new ArrayList<>();

        for(Passenger p : passengers){
            passengerDTOS.add(new PassengerDTO(p));

        }

        return passengerDTOS;

    }

    @Override
    public Passenger verifyPassenger(String token, Long id) throws UserFromTokenNotFoundException, EmployeeNotFoundException, PassengerNotFoundException {

        System.out.println("TOKEN");
        System.out.println(token);

        System.out.println("ID");
        System.out.println(id);

        String username = jwtUtils.getUsernameFromToken(token);

        Account a = accountService.findByUsername(username);

        if(a == null) throw new UserFromTokenNotFoundException();

        Employee employee = a.getEmployee();

        if(employee == null) throw new EmployeeNotFoundException();

        Optional<Passenger> passengerO = passengerService.findById(id);

        if(!passengerO.isPresent()) throw new PassengerNotFoundException();

        Passenger passenger = passengerO.get();
        employee.getVerifiedPassengers().add(passenger);
        save(employee);
        passenger.setVerifiedBy(employee);
        passenger.getAccount().setUserStatus(UserStatus.ACTIVATED);

        accountService.save(passenger.getAccount());
        passenger = passengerService.save(passenger);

        try {
            emailService.sendMail("Your account has been verified!", "Dear" + passenger.getFirstName() +
                    "Your account has officially been verified and you can start using it. Best regards", passenger.getEmail());
        }
        catch (MessagingException m) {
            m.printStackTrace();
            return null;
        }

        return passenger;

    }

    @Override
    public boolean denyPassenger(Long id) throws PassengerNotFoundException {
        Optional<Passenger> passengerO = passengerService.findById(id);

        if(!passengerO.isPresent()) throw new PassengerNotFoundException();

        Passenger passenger = passengerO.get();



        try {
            emailService.sendMail("Your account has been denied!", "Dear" + passenger.getFirstName() +
                    "Your account has been denied. The reason behind this decision is due to validity of the document" +
                    " provided for the passenger type you selected or lack of the said document. Best regards", passenger.getEmail());
        }
        catch (MessagingException m) {
            return false;
        }

        accountService.deleteAccount(passenger.getAccount());
        passengerService.deletePassenger(passenger);


        return true;
    }

}
