package rs.ac.uns.ftn.tim13.tim13.model;

public enum PassengerType{STUDENT, EMPLOYED, PENSIONER, UNEMPLOYED}