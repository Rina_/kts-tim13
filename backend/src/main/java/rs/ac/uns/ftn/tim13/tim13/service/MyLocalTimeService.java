package rs.ac.uns.ftn.tim13.tim13.service;

import rs.ac.uns.ftn.tim13.tim13.model.MyLocalTime;

public interface MyLocalTimeService {

    MyLocalTime save (MyLocalTime myLocalTime);
}
