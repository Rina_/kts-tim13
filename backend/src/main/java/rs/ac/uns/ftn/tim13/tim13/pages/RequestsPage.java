package rs.ac.uns.ftn.tim13.tim13.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class RequestsPage {

    private WebDriver driver;

    @FindBy(xpath = "/html/body/app-root/app-process-requests/h4")
    private WebElement title;

    @FindBy(xpath = "//tr[last()]/td[7]/button")
    private WebElement acceptButton;

    @FindBy(xpath = "//tr[last()]/td[8]/button")
    private WebElement deleteButton;

    @FindBy(xpath = "//*[@id=\"toast-container\"]/div")
    private WebElement success;

    @FindBy(xpath = "/html/body/app-root/app-process-requests/div/h1")
    private WebElement noRequests;


    public RequestsPage(WebDriver driver) {
        this.driver = driver;
    }

    public WebElement getTitle() {
        return title;
    }

    public WebElement getAcceptButton() {
        return acceptButton;
    }

    public void setAcceptButton(WebElement acceptButton) {
        this.acceptButton = acceptButton;
    }

    public WebElement getDeleteButton() {
        return deleteButton;
    }

    public void setDeleteButton(WebElement deleteButton) {
        this.deleteButton = deleteButton;
    }

    public WebElement getSuccess() {
        return success;
    }

    public void setSuccess(WebElement success) {
        this.success = success;
    }

    public WebElement getNoRequests() {
        return noRequests;
    }

    public void setNoRequests(WebElement noRequests) {
        this.noRequests = noRequests;
    }

    public void ensureIsDisplayedTitle(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(title));
    }

    public void ensureIsDisplayedAccept(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(acceptButton));
    }

    public void ensureIsDisplayedDelete(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(deleteButton));
    }

    public void ensureIsDisplayedSuccess(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(success));
    }

    public void ensureIsDisplayedNoRequests(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(noRequests));
    }

    public int getRequestsTableSize() {
        return driver.findElements(By.cssSelector("tr")).size();
    }

    public void ensureIsDeleted(int previousNoOfRequests) {
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.invisibilityOfElementLocated(
                        By.xpath("//tr[" + (previousNoOfRequests + 1) + "]")));
    }

}
