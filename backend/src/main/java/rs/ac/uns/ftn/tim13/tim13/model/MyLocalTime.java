package rs.ac.uns.ftn.tim13.tim13.model;

import javax.persistence.*;
import java.time.LocalTime;

@Entity
@Table(name = "myLocalTime")
public class MyLocalTime {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    @Column(nullable = false)
    private LocalTime time;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }

    public MyLocalTime(LocalTime time) {
        this.time = time;
    }

    public MyLocalTime() {
    }
}
