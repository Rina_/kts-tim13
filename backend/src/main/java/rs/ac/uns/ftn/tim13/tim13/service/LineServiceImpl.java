package rs.ac.uns.ftn.tim13.tim13.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.tim13.tim13.model.Line;
import rs.ac.uns.ftn.tim13.tim13.repository.LineRepository;

@Service
public class LineServiceImpl implements LineService {

    @Autowired
    private LineRepository lineRepository;

    @Override
    public Line save(Line line) {
        return lineRepository.save(line);
    }
}
