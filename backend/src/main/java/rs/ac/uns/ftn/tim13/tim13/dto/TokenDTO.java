package rs.ac.uns.ftn.tim13.tim13.dto;

public class TokenDTO {
    private String value;

    public TokenDTO(String value) {
        this.value = value;
    }

    public TokenDTO() {
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
