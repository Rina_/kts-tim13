package rs.ac.uns.ftn.tim13.tim13.service;

import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.tim13.tim13.model.Schedule;

public interface ScheduleService {

    Schedule save (Schedule schedule);
}
