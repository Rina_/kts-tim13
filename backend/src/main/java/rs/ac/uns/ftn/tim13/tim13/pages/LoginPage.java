package rs.ac.uns.ftn.tim13.tim13.pages;

import com.google.common.base.Preconditions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import rs.ac.uns.ftn.tim13.tim13.model.Passenger;

public class LoginPage {

    private WebDriver driver;

    @FindBy(xpath = "//*[@id=\"username\"]")
    private WebElement username;

    @FindBy(xpath = "//*[@id=\"password\"]")
    private WebElement password;

    @FindBy(xpath = "/html/body/app-root/app-login/div/form/div[3]/button")
    private WebElement login;

    @FindBy(xpath = "//*[@id=\"toast-container\"]/div")
    private WebElement badCredentials;

    @FindBy(xpath = "/html/body/app-root/app-login/div/h2")
    private WebElement title;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
    }

    public WebElement getUsername() {
        return username;
    }

    public void setUsername(String username) {
        WebElement el = getUsername();
        el.clear();
        el.sendKeys(username);
    }

    public WebElement getPassword() {
        return password;
    }

    public void setPassword(String password) {
        WebElement el = getPassword();
        el.clear();
        el.sendKeys(password);
    }

    public WebElement getLogin() {
        return login;
    }

    public void setLogin(WebElement login) {
        this.login = login;
    }

    public WebElement getBadCredentials() {
        return badCredentials;
    }

    public void setBadCredentials(WebElement badCredentials) {
        this.badCredentials = badCredentials;
    }

    public WebElement getTitle() {
        return title;
    }

    public void setTitle(WebElement title) {
        this.title = title;
    }

    public void ensureIsDisplayedUsername(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(username));
    }

    public void ensureIsDisplayedPassword(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(password));
    }

    public void ensureIsDisplayedLogin(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(login));
    }

    public void ensureIsDisplayedBadCredentials(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(badCredentials));
    }


    public void ensureIsDisplayedTitle(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(title));
    }

    public EmployeePage loginAsEmployee() {

        ensureIsDisplayedUsername();
        setUsername("Lala1");

        ensureIsDisplayedPassword();
        setPassword("123456");

        getLogin().click();
        return new EmployeePage(driver);
    }

    public AdminPage loginAsAdmin(){

        ensureIsDisplayedUsername();
        setUsername("Mica");

        ensureIsDisplayedPassword();
        setPassword("123456");

        getLogin().click();
        return new AdminPage(driver);
    }

    public PassengerPage loginAsPassenger(){

        ensureIsDisplayedUsername();
        setUsername("Makalina");

        ensureIsDisplayedPassword();
        setPassword("123456");

        getLogin().click();
        return new PassengerPage(driver);
    }
}
