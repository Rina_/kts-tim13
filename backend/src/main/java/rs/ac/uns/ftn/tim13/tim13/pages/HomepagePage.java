package rs.ac.uns.ftn.tim13.tim13.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomepagePage {

    private WebDriver driver;

    @FindBy(xpath = "/html/body/app-root/navbar/nav/div[2]/a")
    private WebElement Login;

    @FindBy(xpath = "/html/body/app-root/app-homepage/button")
    private WebElement Register;

    public HomepagePage(WebDriver driver) {
        this.driver = driver;
    }

    public WebElement getLogin() {
        return Login;
    }

    public void setLogin(WebElement login) {
        Login = login;
    }

    public WebElement getRegister() {
        return Register;
    }

    public void setRegister(WebElement register) {
        Register = register;
    }

    public void ensureIsDisplayedLogin(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(Login));
    }

    public void ensureIsDisplayedRegister(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(Register));
    }


}
