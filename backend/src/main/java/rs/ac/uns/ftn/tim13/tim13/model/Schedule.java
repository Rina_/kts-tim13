package rs.ac.uns.ftn.tim13.tim13.model;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "schedule")
public class Schedule {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    @Column(nullable = false)
    private Date dateFrom;

    @Enumerated(EnumType.STRING)
    private DayType dayType;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private Line line;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private List<MyLocalTime> localTimes;

    public Schedule() {

        localTimes = new ArrayList<>();
    }

    public Schedule(Date dateFrom, DayType dayType, List<MyLocalTime> localTimes) {
        this.dateFrom = dateFrom;
        this.dayType = dayType;
        this.line = line;
        this.localTimes = localTimes;
    }

    public Long getId() {

        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public DayType getDayType() {
        return dayType;
    }

    public void setDayType(DayType dayType) {
        this.dayType = dayType;
    }

    public Line getLine() {
        return line;
    }

    public void setLine(Line line) {
        this.line = line;
    }

    public List<MyLocalTime> getLocalTimes() {
        return localTimes;
    }

    public void setLocalTimes(List<MyLocalTime> localTimes) {
        this.localTimes = localTimes;
    }
}
