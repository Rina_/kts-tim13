package rs.ac.uns.ftn.tim13.tim13.model;

import org.hibernate.type.DateType;

import javax.persistence.*;

@Entity
@Table(name = "ticket")
public class Ticket {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    @Column(nullable = false)
    private DateType startDate;

    @Column(nullable = false)
    private DateType endDate;

    @Column(nullable = false)
    private double price;

    @Column(nullable = false)
    private boolean activated;

    @Enumerated(EnumType.STRING)
    private PassengerType passengerType;

    @Enumerated(EnumType.STRING)
    private TicketType TicketType;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private Passenger passenger;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private Zone zone;

    public Ticket(DateType startDate, DateType endDate, double price, boolean activated, PassengerType passengerType, TicketType ticketType, Passenger passenger, Zone zone) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.price = price;
        this.activated = activated;
        this.passengerType = passengerType;
        TicketType = ticketType;
        this.passenger = passenger;
        this.zone = zone;
    }

    public Ticket() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public DateType getStartDate() {
        return startDate;
    }

    public void setStartDate(DateType startDate) {
        this.startDate = startDate;
    }

    public DateType getEndDate() {
        return endDate;
    }

    public void setEndDate(DateType endDate) {
        this.endDate = endDate;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public boolean isActivated() {
        return activated;
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
    }

    public PassengerType getPassengerType() {
        return passengerType;
    }

    public void setPassengerType(PassengerType passengerType) {
        this.passengerType = passengerType;
    }

    public rs.ac.uns.ftn.tim13.tim13.model.TicketType getTicketType() {
        return TicketType;
    }

    public void setTicketType(rs.ac.uns.ftn.tim13.tim13.model.TicketType ticketType) {
        TicketType = ticketType;
    }

    public Passenger getPassenger() {
        return passenger;
    }

    public void setPassenger(Passenger passenger) {
        this.passenger = passenger;
    }

    public Zone getZone() {
        return zone;
    }

    public void setZone(Zone zone) {
        this.zone = zone;
    }
}
