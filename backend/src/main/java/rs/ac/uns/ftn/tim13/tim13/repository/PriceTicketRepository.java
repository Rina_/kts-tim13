package rs.ac.uns.ftn.tim13.tim13.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.ac.uns.ftn.tim13.tim13.model.PriceTicket;
import rs.ac.uns.ftn.tim13.tim13.model.TicketType;

public interface PriceTicketRepository extends JpaRepository<PriceTicket, Long> {

    PriceTicket findByTicketType(TicketType tt);
}
