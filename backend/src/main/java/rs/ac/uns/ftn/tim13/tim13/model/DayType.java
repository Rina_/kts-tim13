package rs.ac.uns.ftn.tim13.tim13.model;

public enum DayType {WORKDAY, SATURDAY, SUNDAY}