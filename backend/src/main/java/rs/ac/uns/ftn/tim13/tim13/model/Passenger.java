package rs.ac.uns.ftn.tim13.tim13.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "passenger")
public class Passenger {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToOne(mappedBy = "passenger")
    @JsonBackReference
    private Account account;

    @Column(nullable = false)
    private String firstName;

    @Column(nullable = false)
    private String lastName;

    @Column(nullable = false)
    private String email;

    @Column(nullable = false)
    private String address;

    @Column(nullable = false)
    private String confirmation;

    @Column(nullable = false)
    private Date expirationDate;

    @Enumerated(EnumType.STRING)
    private PassengerType passengerType;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private List<Ticket> tickets;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private Employee verifiedBy;

    public Passenger() {
    }

    public Passenger(Account account, String firstName, String lastName, String email, String address, String confirmation,
                     Date expirationDate, PassengerType passengerType, List<Ticket> tickets, Employee verifiedBy) {
        this.account = account;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.address = address;
        this.confirmation = confirmation;
        this.expirationDate = expirationDate;
        this.passengerType = passengerType;
        this.tickets = tickets;
        this.verifiedBy = verifiedBy;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getConfirmation() {
        return confirmation;
    }

    public void setConfirmation(String confirmation) {
        this.confirmation = confirmation;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public PassengerType getPassengerType() {
        return passengerType;
    }

    public void setPassengerType(PassengerType passengerType) {
        this.passengerType = passengerType;
    }

    public List<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(List<Ticket> tickets) {
        this.tickets = tickets;
    }

    public Employee getVerifiedBy() {
        return verifiedBy;
    }

    public void setVerifiedBy(Employee verifiedBy) {
        this.verifiedBy = verifiedBy;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Passenger(String firstName, String lastName, String email, String address, String confirmation,
                     Date expirationDate, PassengerType passengerType, Employee verifiedBy) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.address = address;
        this.confirmation = confirmation;
        this.expirationDate = expirationDate;
        this.passengerType = passengerType;
        this.verifiedBy = verifiedBy;
    }
}
