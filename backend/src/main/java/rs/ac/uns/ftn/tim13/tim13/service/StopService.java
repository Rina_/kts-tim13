package rs.ac.uns.ftn.tim13.tim13.service;

import rs.ac.uns.ftn.tim13.tim13.model.Stop;

public interface StopService {

    Stop findByName (String name);
    Stop findByXandY (double x, double y);
    Stop save (Stop stop);
}
