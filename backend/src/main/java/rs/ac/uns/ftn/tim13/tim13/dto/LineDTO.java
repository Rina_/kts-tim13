package rs.ac.uns.ftn.tim13.tim13.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class LineDTO implements Serializable {

    private String lineName;
    private String zoneName;
    private List<StopDTO> stops;
    private List<ScheduleDTO> schedules;


    public LineDTO(){
        stops = new ArrayList<>();
        schedules = new ArrayList<>();
    }

    public LineDTO(String lineName, String zoneName, List<StopDTO> stops, List<ScheduleDTO> schedules) {
        this.lineName = lineName;
        this.zoneName = zoneName;
        this.stops = stops;
        this.schedules = schedules;
    }

    public List<ScheduleDTO> getSchedules() {
        return schedules;
    }

    public void setSchedules(List<ScheduleDTO> schedules) {
        this.schedules = schedules;
    }

    public String getLineName() {
        return lineName;
    }

    public void setLineName(String lineName) {
        this.lineName = lineName;
    }

    public String getZoneName() {
        return zoneName;
    }

    public void setZoneName(String zoneName) {
        this.zoneName = zoneName;
    }

    public List<StopDTO> getStops() {
        return stops;
    }

    public void setStops(List<StopDTO> stops) {
        this.stops = stops;
    }

    @Override
    public String toString() {
        return "LineDTO{" +
                "lineName='" + lineName + '\'' +
                ", zoneName='" + zoneName + '\'' +
                ", stops=" + stops +
                ", schedules=" + schedules +
                '}';
    }
}
