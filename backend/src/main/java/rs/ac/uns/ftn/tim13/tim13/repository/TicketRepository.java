package rs.ac.uns.ftn.tim13.tim13.repository;
import java.util.ArrayList;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.ac.uns.ftn.tim13.tim13.model.Ticket;

public interface TicketRepository extends JpaRepository<Ticket, Long> {

    Optional<Ticket> findbyId(Long Id);
    ArrayList<Ticket> findbyUsername(String username);
}
