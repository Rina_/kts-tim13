package rs.ac.uns.ftn.tim13.tim13.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class NewEmployeePage {

    private WebDriver driver;

    @FindBy(xpath = "/html/body/app-root/app-new-employee/div[2]/div/div/div/div[1]/h3")
    private WebElement title;

    @FindBy(xpath = "//*[@id=\"firstName\"]")
    private WebElement firstName;

    @FindBy(xpath = "//*[@id=\"lastName\"]")
    private WebElement lastName;

    @FindBy(xpath = "//*[@id=\"username\"]")
    private WebElement username;

    @FindBy(xpath = "//*[@id=\"password\"]")
    private WebElement password;

    @FindBy(xpath = "//*[@id=\"confirmPassword\"]")
    private WebElement passwordConfirmation;

    @FindBy(xpath = "//*[@id=\"email\"]")
    private WebElement email;

    @FindBy(xpath = "//*[@id=\"address\"]")
    private WebElement address;

    @FindBy(xpath = "//*[@id=\"phoneNumber\"]")
    private WebElement phoneNumber;

    @FindBy(xpath = "//*[@id=\"authority\"]")
    private WebElement authority;

    @FindBy(xpath = "//*[@id=\"authority\"]/option[1]")
    private WebElement admin;

    @FindBy(xpath = "//*[@id=\"authority\"]/option[2]")
    private WebElement employee;

    @FindBy(xpath = "/html/body/app-root/app-new-employee/div[2]/div/div/div/div[2]/form/button")
    private WebElement register;

    @FindBy(xpath = "//*[@id=\"firstNameRequiredError\"]")
    private WebElement firstNameRequired;

    @FindBy(xpath = "//*[@id=\"lastNameRequiredError\"]")
    private WebElement lastNameRequired;

    @FindBy(xpath = "//*[@id=\"usernameRequiredError\"]")
    private WebElement usernameRequired;

    @FindBy(xpath = "//*[@id=\"passwordRequiredError\"]")
    private WebElement passwordRequired;

    @FindBy(xpath = "//*[@id=\"confirmPasswordRequiredError\"]")
    private WebElement passwordConfirmationRequired;

    @FindBy(xpath = "//*[@id=\"passwordDontMatchError\"]")
    private WebElement passwordsDontMatch;

    @FindBy(xpath = "//*[@id=\"emailRequiredError\"]")
    private WebElement emailRequired;

    @FindBy(xpath = "//*[@id=\"emailValidError\"]")
    private WebElement invalidEmail;

    @FindBy(xpath = "//*[@id=\"addressRequiredError\"]")
    private WebElement addressRequired;

    @FindBy(xpath = "//*[@id=\"phoneNumberRequiredError\"]")
    private WebElement phoneNumberRequired;

    @FindBy(xpath = "//*[@id=\"toast-container\"]/div")
    private WebElement usernameExists;

    @FindBy(xpath = "/html/body")
    private WebElement background;

    public NewEmployeePage(WebDriver driver) {
        this.driver = driver;
    }

    public WebElement getBackground() {
        return background;
    }

    public WebElement getTitle() {
        return title;
    }

    public WebElement getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
       WebElement el = getFirstName();
       el.clear();
       el.sendKeys(firstName);
    }

    public WebElement getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        WebElement el = getLastName();
        el.clear();
        el.sendKeys(lastName);
    }

    public WebElement getUsername() {
        return username;
    }

    public void setUsername(String username) {
        WebElement el = getUsername();
        el.clear();
        el.sendKeys(username);
    }

    public WebElement getPassword() {
        return password;
    }

    public void setPassword(String password) {
        WebElement el = getPassword();
        el.clear();
        el.sendKeys(password);
    }

    public WebElement getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        WebElement el = getPasswordConfirmation();
        el.clear();
        el.sendKeys(passwordConfirmation);
    }

    public WebElement getEmail() {
        return email;
    }

    public void setEmail(String email) {
        WebElement el = getEmail();
        el.clear();
        el.sendKeys(email);
    }

    public WebElement getAddress() {
        return address;
    }

    public void setAddress(String address) {
        WebElement el = getAddress();
        el.clear();
        el.sendKeys(address);
    }

    public WebElement getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        WebElement el = getPhoneNumber();
        el.clear();
        el.sendKeys(phoneNumber);
    }

    public WebElement getAuthority() {
        return authority;
    }

    public void setAuthority(WebElement authority) {
        this.authority = authority;
    }

    public WebElement getAdmin() {
        return admin;
    }

    public void setAdmin(WebElement admin) {
        this.admin = admin;
    }

    public WebElement getEmployee() {
        return employee;
    }

    public void setEmployee(WebElement employee) {
        this.employee = employee;
    }

    public WebElement getRegister() {
        return register;
    }

    public void setRegister(WebElement register) {
        this.register = register;
    }

    public WebElement getFirstNameRequired() {
        return firstNameRequired;
    }

    public void setFirstNameRequired(WebElement firstNameRequired) {
        this.firstNameRequired = firstNameRequired;
    }

    public WebElement getLastNameRequired() {
        return lastNameRequired;
    }

    public void setLastNameRequired(WebElement lastNameRequired) {
        this.lastNameRequired = lastNameRequired;
    }

    public WebElement getUsernameRequired() {
        return usernameRequired;
    }

    public void setUsernameRequired(WebElement usernameRequired) {
        this.usernameRequired = usernameRequired;
    }

    public WebElement getPasswordRequired() {
        return passwordRequired;
    }

    public void setPasswordRequired(WebElement passwordRequired) {
        this.passwordRequired = passwordRequired;
    }

    public WebElement getPasswordConfirmationRequired() {
        return passwordConfirmationRequired;
    }

    public void setPasswordConfirmationRequired(WebElement passwordConfirmationRequired) {
        this.passwordConfirmationRequired = passwordConfirmationRequired;
    }

    public WebElement getPasswordsDontMatch() {
        return passwordsDontMatch;
    }

    public void setPasswordsDontMatch(WebElement passwordsDontMatch) {
        this.passwordsDontMatch = passwordsDontMatch;
    }

    public WebElement getEmailRequired() {
        return emailRequired;
    }

    public void setEmailRequired(WebElement emailRequired) {
        this.emailRequired = emailRequired;
    }

    public WebElement getInvalidEmail() {
        return invalidEmail;
    }

    public void setInvalidEmail(WebElement invalidEmail) {
        this.invalidEmail = invalidEmail;
    }

    public WebElement getAddressRequired() {
        return addressRequired;
    }

    public void setAddressRequired(WebElement addressRequired) {
        this.addressRequired = addressRequired;
    }

    public WebElement getPhoneNumberRequired() {
        return phoneNumberRequired;
    }

    public void setPhoneNumberRequired(WebElement phoneNumberRequired) {
        this.phoneNumberRequired = phoneNumberRequired;
    }

    public WebElement getUsernameExists() {
        return usernameExists;
    }

    public void setUsernameExists(WebElement usernameExists) {
        this.usernameExists = usernameExists;
    }

    public void ensureIsDisplayedFirstName(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(firstName));
    }

    public void ensureIsDisplayedLastName(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(lastName));
    }

    public void ensureIsDisplayedUsername(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(username));
    }

    public void ensureIsDisplayedPassword(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(password));
    }

    public void ensureIsDisplayedPasswordConfirmation(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(passwordConfirmation));
    }

    public void ensureIsDisplayedEmail(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(email));
    }

    public void ensureIsDisplayedAddress(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(address));
    }

    public void ensureIsDisplayedPhoneNumber(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(phoneNumber));
    }

    public void ensureIsDisplayedAuthority(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(authority));
    }

    public void ensureIsDisplayedAdmin(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(admin));
    }

    public void ensureIsDisplayedEmployee(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(employee));
    }

    public void ensureIsDisplayedRegister(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(register));
    }

    public void ensureIsDisplayedFirstNameRequired(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(firstNameRequired));
    }

    public void ensureIsDisplayedLastNameRequired(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(lastNameRequired));
    }

    public void ensureIsDisplayedUsernameRequired(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(usernameRequired));
    }

    public void ensureIsDisplayedPasswordRequired(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(passwordRequired));
    }

    public void ensureIsDisplayedPasswordConfirmationRequired(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(passwordConfirmationRequired));
    }

    public void ensureIsDisplayedPasswordsDontMatch(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(passwordsDontMatch));
    }

    public void ensureIsDisplayedEmailRequired(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(emailRequired));
    }

    public void ensureIsDisplayedInvalidEmail(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(invalidEmail));
    }

    public void ensureIsDisplayedAddressRequired(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(addressRequired));
    }

    public void ensureIsDisplayedPhoneNumberRequired(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(phoneNumberRequired));
    }

    public void ensureIsDisplayedUsernameExists(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(usernameExists));
    }

    public void ensureIsDisplayeTitle(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(title));
    }

    public void ensureIsDisplayeBackground(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(background));
    }



}
