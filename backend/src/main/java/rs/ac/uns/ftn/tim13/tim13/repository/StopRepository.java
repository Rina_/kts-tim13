package rs.ac.uns.ftn.tim13.tim13.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import rs.ac.uns.ftn.tim13.tim13.model.Stop;

public interface StopRepository extends JpaRepository<Stop, Long>{

    @Query(value = "SELECT S FROM Stop S where S.xCoordinate = :x and S.yCoordinate = :y")
    Stop findByXandY(@Param("x") double x, @Param("y")double y);

    Stop findByName(String name);

}
