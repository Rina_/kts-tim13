package rs.ac.uns.ftn.tim13.tim13.service;

import rs.ac.uns.ftn.tim13.tim13.model.Authority;

public interface AuthorityService {

    Authority getByName(String name);

    Authority save(Authority authority);
}
