package rs.ac.uns.ftn.tim13.tim13.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class EmployeePage {

    private WebDriver driver;

    @FindBy(xpath = "/html/body/app-root/app-employee-homepage/button")
    private WebElement processRequests;

    public EmployeePage(WebDriver driver) {
        this.driver = driver;
    }

    public WebElement getProcessRequests() {
        return processRequests;
    }

    public void setProcessRequests(WebElement processRequests) {
        this.processRequests = processRequests;
    }

    public void ensureIsDisplayedProcessRequests(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(processRequests));
    }
}
