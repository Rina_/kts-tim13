package rs.ac.uns.ftn.tim13.tim13.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import rs.ac.uns.ftn.tim13.tim13.model.Employee;

import java.util.List;

public interface EmployeeRepository extends JpaRepository<Employee, Long>{

    @Query(value = "SELECT e FROM Employee e WHERE e.account.username = :username", nativeQuery = false)
    Employee findByUsername(@Param("username") String username);

    @Query(value = "SELECT MAX(employmentNumber) from Employee e", nativeQuery = false)
    long findEmploymentNumber();




}
