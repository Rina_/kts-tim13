package rs.ac.uns.ftn.tim13.tim13.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.tim13.tim13.model.MyLocalTime;
import rs.ac.uns.ftn.tim13.tim13.repository.MyLocalTimeRepository;

@Service
public class MyLocalTimeServiceImpl implements MyLocalTimeService{

    @Autowired
    private MyLocalTimeRepository myLocalTimeRepository;

    @Override
    public MyLocalTime save(MyLocalTime myLocalTime) {
        return myLocalTimeRepository.save(myLocalTime);
    }
}
