package rs.ac.uns.ftn.tim13.tim13.model;

public enum UserStatus{ACTIVATED, DEACTIVATED, PENDING}