package rs.ac.uns.ftn.tim13.tim13.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PassengerPage {

    private WebDriver driver;

    @FindBy(xpath = "/html/body/app-root/app-passenger-homepage/p")
    private WebElement welcome;

    public PassengerPage(WebDriver driver) {
        this.driver = driver;
    }

    public WebElement getWelcome() {
        return welcome;
    }

    public void setWelcome(WebElement welcome) {
        this.welcome = welcome;
    }

    public void ensureIsDisplayedWelcome(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(welcome));
    }
}
