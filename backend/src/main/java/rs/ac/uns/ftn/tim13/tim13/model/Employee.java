package rs.ac.uns.ftn.tim13.tim13.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "employee")
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;


    @OneToOne(mappedBy = "employee")
    @JsonBackReference
    private Account account;

    @Column(nullable = false)
    private String firstName;

    @Column(nullable = false)
    private String lastName;

    @Column(nullable = false)
    private String email;

    @Column(nullable = false)
    private String address;

    @Column(nullable = false)
    private Long phoneNumber;

    @Column(nullable = false)
    private Date employmentDate;

    @Column(nullable = false)
    private Long employmentNumber;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private List<Passenger> verifiedPassengers;

    public Employee() {
    }

    public Employee(String firstName, String lastName, String email, String address, Long phoneNumber, Date employmentDate, Long employmentNumber) {

        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.employmentDate = employmentDate;
        this.employmentNumber = employmentNumber;
    }

    public Employee(Long id, String firstName, String lastName, String email, String address, Long phoneNumber, Date employmentDate, Long employmentNumber) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.employmentDate = employmentDate;
        this.employmentNumber = employmentNumber;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Passenger> getVerifiedPassengers() {
        return verifiedPassengers;
    }

    public void setVerifiedPassengers(List<Passenger> verifiedPassengers) {
        this.verifiedPassengers = verifiedPassengers;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Long getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(Long phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Date getEmploymentDate() {
        return employmentDate;
    }

    public void setEmploymentDate(Date employmentDate) {
        this.employmentDate = employmentDate;
    }

    public Long getEmploymentNumber() {
        return employmentNumber;
    }

    public void setEmploymentNumber(Long employmentNumber) {
        this.employmentNumber = employmentNumber;
    }


}
