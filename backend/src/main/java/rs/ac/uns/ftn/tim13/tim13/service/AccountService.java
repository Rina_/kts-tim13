package rs.ac.uns.ftn.tim13.tim13.service;

import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.tim13.tim13.dto.LoginDTO;
import rs.ac.uns.ftn.tim13.tim13.dto.TokenDTO;
import rs.ac.uns.ftn.tim13.tim13.exceptions.UsernameExistsException;
import rs.ac.uns.ftn.tim13.tim13.model.Account;

public interface AccountService {
	Account register(Account u) throws UsernameExistsException;
	Account findByUsername(String username);

	Account save(Account account);

	void deleteAccount(Account account);

	TokenDTO login(LoginDTO loginDTO);

}
