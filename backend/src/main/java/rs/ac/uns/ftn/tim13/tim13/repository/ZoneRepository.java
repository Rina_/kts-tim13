package rs.ac.uns.ftn.tim13.tim13.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rs.ac.uns.ftn.tim13.tim13.model.Zone;

public interface ZoneRepository extends JpaRepository<Zone, Long>{

    Zone findByName(String name);
}
