package rs.ac.uns.ftn.tim13.tim13.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AdminPage {

    private WebDriver driver;

    @FindBy(xpath = "/html/body/app-root/app-admin-homepage/button[2]")
    private WebElement processRequests;

    @FindBy(xpath = "/html/body/app-root/app-admin-homepage/button[1]")
    private WebElement addEmployee;

    public AdminPage(WebDriver driver) {
        this.driver = driver;
    }

    public WebElement getProcessRequests() {
        return processRequests;
    }

    public WebElement getAddEmployee() {
        return addEmployee;
    }

    public void ensureIsDisplayedProcessRequests(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(processRequests));
    }

    public void ensureIsDisplayedAddEmployee(){
        (new WebDriverWait(driver, 10))
                .until(ExpectedConditions.visibilityOf(addEmployee));
    }
}
