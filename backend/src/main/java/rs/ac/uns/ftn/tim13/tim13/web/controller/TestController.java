package rs.ac.uns.ftn.tim13.tim13.web.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/test")
public class TestController {

    @GetMapping(path = "/a")
    public ResponseEntity Test(){
        return new ResponseEntity(new String("AFSJFSA"), HttpStatus.OK);
    }
}
