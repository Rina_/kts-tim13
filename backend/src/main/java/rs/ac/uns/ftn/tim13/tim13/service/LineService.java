package rs.ac.uns.ftn.tim13.tim13.service;

import org.springframework.stereotype.Service;
import rs.ac.uns.ftn.tim13.tim13.model.Line;

public interface LineService {

    Line save(Line line);
}
