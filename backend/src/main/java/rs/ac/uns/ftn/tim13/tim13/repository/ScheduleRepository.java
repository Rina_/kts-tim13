package rs.ac.uns.ftn.tim13.tim13.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import rs.ac.uns.ftn.tim13.tim13.model.Schedule;

public interface ScheduleRepository extends JpaRepository<Schedule,Long> {

}
