package rs.ac.uns.ftn.tim13.tim13.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import rs.ac.uns.ftn.tim13.tim13.model.Passenger;

import java.util.List;
import java.util.Optional;

public interface PassengerRepository extends JpaRepository<Passenger, Long> {

    @Query(value = "SELECT P FROM Passenger P WHERE P.verifiedBy = null and P.passengerType not like 'EMPLOYED'", nativeQuery = false)
    List<Passenger> findUnverifiedPassengers();

    Optional<Passenger> findById(Long id);



}
