package rs.ac.uns.ftn.tim13.tim13.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "line")
public class Line {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    @Column(nullable = false)
    private String name;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private Zone zone;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private List<Stop> stops;

    @OneToMany(mappedBy = "line", fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private List<Schedule> schedules;

    @OneToMany(mappedBy = "line", fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private List<Transport> transports;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Zone getZone() {
        return zone;
    }

    public void setZone(Zone zone) {
        this.zone = zone;
    }

    public List<Stop> getStops() {
        return stops;
    }

    public void setStops(List<Stop> stops) {
        this.stops = stops;
    }

    public List<Schedule> getSchedules() {
        return schedules;
    }

    public void setSchedules(List<Schedule> schedules) {
        this.schedules = schedules;
    }

    public List<Transport> getTransports() {
        return transports;
    }

    public void setTransports(List<Transport> transports) {
        this.transports = transports;
    }

    public Line() {
        stops = new ArrayList<>();
        schedules = new ArrayList<>();
        transports = new ArrayList<>();
    }

    public Line(String name, Zone zone, List<Stop> stops) {
        this.name = name;
        this.zone = zone;
        this.stops = stops;
    }
}
