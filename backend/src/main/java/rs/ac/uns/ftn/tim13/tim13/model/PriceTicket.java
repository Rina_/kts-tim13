package rs.ac.uns.ftn.tim13.tim13.model;

import org.hibernate.type.DateType;

import javax.persistence.*;

@Entity
@Table(name = "priceTicket")
public class PriceTicket {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    @Column(nullable = false)
    private DateType dateFrom;

    @Enumerated(EnumType.STRING)
    private PassengerType passengerType;

    @Enumerated(EnumType.STRING)
    private TicketType ticketType;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    private Zone zone;

    @Column(nullable = false)
    private double price;

    public PriceTicket(DateType dateFrom, PassengerType passengerType, TicketType ticketType, Zone zone, double price) {
        this.dateFrom = dateFrom;
        this.passengerType = passengerType;
        this.ticketType = ticketType;
        this.zone = zone;
        this.price = price;
    }

    public PriceTicket() {

    }

    public Long getId() {

        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public DateType getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(DateType dateFrom) {
        this.dateFrom = dateFrom;
    }

    public PassengerType getPassengerType() {
        return passengerType;
    }

    public void setPassengerType(PassengerType passengerType) {
        this.passengerType = passengerType;
    }

    public TicketType getTicketType() {
        return ticketType;
    }

    public void setTicketType(TicketType ticketType) {
        this.ticketType = ticketType;
    }

    public Zone getZone() {
        return zone;
    }

    public void setZone(Zone zone) {
        this.zone = zone;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
