package rs.ac.uns.ftn.tim13.tim13.e2e;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import rs.ac.uns.ftn.tim13.tim13.pages.*;

import static org.testng.AssertJUnit.assertEquals;

public class LoginTest {

    private WebDriver browser;

    private HomepagePage homepagePage;

    private LoginPage loginPage;

    private EmployeePage employeePage;

    private AdminPage adminPage;

    private PassengerPage passengerPage;

    @BeforeMethod
    public void setupSelenium(){

        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Windows User\\Desktop\\chromedriver.exe");
        browser = new ChromeDriver();

        browser.manage().window().maximize();

        browser.navigate().to("http://localhost:4200/homepage");

        homepagePage = PageFactory.initElements(browser, HomepagePage.class);
        loginPage = PageFactory.initElements(browser, LoginPage.class);
        employeePage = PageFactory.initElements(browser, EmployeePage.class);
        adminPage = PageFactory.initElements(browser, AdminPage.class);
        passengerPage = PageFactory.initElements(browser, PassengerPage.class);

        homepagePage.getLogin().click();

    }

    @Test
    public void LoginSuccessEmployee(){

        assertEquals("http://localhost:4200/login", browser.getCurrentUrl());

        loginPage.ensureIsDisplayedUsername();

        loginPage.setUsername("Lala1");

        loginPage.ensureIsDisplayedPassword();

        loginPage.setPassword("123456");

        loginPage.ensureIsDisplayedLogin();

        loginPage.getLogin().click();

        employeePage.ensureIsDisplayedProcessRequests();

        assertEquals("http://localhost:4200/employee_homepage", browser.getCurrentUrl());

    }

    @Test
    public void LoginSuccessAdmin(){

        assertEquals("http://localhost:4200/login", browser.getCurrentUrl());

        loginPage.ensureIsDisplayedUsername();

        loginPage.setUsername("Mica");

        loginPage.ensureIsDisplayedPassword();

        loginPage.setPassword("123456");

        loginPage.ensureIsDisplayedLogin();

        loginPage.getLogin().click();

        adminPage.ensureIsDisplayedProcessRequests();

        assertEquals("http://localhost:4200/admin_homepage", browser.getCurrentUrl());

    }

    @Test
    public void LoginSuccessPassenger(){

        assertEquals("http://localhost:4200/login", browser.getCurrentUrl());

        loginPage.ensureIsDisplayedUsername();

        loginPage.setUsername("Makalina");

        loginPage.ensureIsDisplayedPassword();

        loginPage.setPassword("123456");

        loginPage.ensureIsDisplayedLogin();

        loginPage.getLogin().click();

        passengerPage.ensureIsDisplayedWelcome();

        assertEquals("http://localhost:4200/passenger_homepage", browser.getCurrentUrl());

    }

    @Test
    public void LoginFailureUsername(){

        assertEquals("http://localhost:4200/login", browser.getCurrentUrl());

        loginPage.ensureIsDisplayedUsername();

        loginPage.setUsername("Glupost");

        loginPage.ensureIsDisplayedPassword();

        loginPage.setPassword("123456");

        loginPage.ensureIsDisplayedLogin();

        loginPage.getLogin().click();

        loginPage.ensureIsDisplayedBadCredentials();

    }

    @Test
    public void LoginFailurePassword(){

        assertEquals("http://localhost:4200/login", browser.getCurrentUrl());

        loginPage.ensureIsDisplayedUsername();

        loginPage.setUsername("Lala1");

        loginPage.ensureIsDisplayedPassword();

        loginPage.setPassword("Glupost");

        loginPage.ensureIsDisplayedLogin();

        loginPage.getLogin().click();

        loginPage.ensureIsDisplayedBadCredentials();

    }

    @Test
    public void LoginFailureBoth(){

        assertEquals("http://localhost:4200/login", browser.getCurrentUrl());

        loginPage.ensureIsDisplayedUsername();

        loginPage.setUsername("Glupost");

        loginPage.ensureIsDisplayedPassword();

        loginPage.setPassword("Glupost");

        loginPage.ensureIsDisplayedLogin();

        loginPage.getLogin().click();

        loginPage.ensureIsDisplayedBadCredentials();

    }

    @Test
    public void LoginFailureEmpty(){

        assertEquals("http://localhost:4200/login", browser.getCurrentUrl());

        loginPage.ensureIsDisplayedUsername();

        loginPage.setUsername("");

        loginPage.ensureIsDisplayedPassword();

        loginPage.setPassword("");

        loginPage.ensureIsDisplayedLogin();

        loginPage.getLogin().click();

        loginPage.ensureIsDisplayedBadCredentials();

    }

    @Test
    public void LoginFailurePendingUser(){

        assertEquals("http://localhost:4200/login", browser.getCurrentUrl());

        loginPage.ensureIsDisplayedUsername();

        loginPage.setUsername("Lalalica");

        loginPage.ensureIsDisplayedPassword();

        loginPage.setPassword("123456");

        loginPage.ensureIsDisplayedLogin();

        loginPage.getLogin().click();

        loginPage.ensureIsDisplayedBadCredentials();

    }



}
