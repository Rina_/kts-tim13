package rs.ac.uns.ftn.tim13.tim13.e2e;

import org.apache.regexp.RE;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import rs.ac.uns.ftn.tim13.tim13.pages.*;

import static org.testng.AssertJUnit.assertEquals;

public class ProcessRequestsTest {

    private WebDriver browser;

    private HomepagePage homepagePage;

    private LoginPage loginPage;

    private RequestsPage requestsPage;

    private AdminPage adminPage;

    @BeforeMethod
    public void setupSelenium(){

        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Windows User\\Desktop\\chromedriver.exe");
        browser = new ChromeDriver();

        browser.manage().window().maximize();

        browser.navigate().to("http://localhost:4200/homepage");

        homepagePage = PageFactory.initElements(browser, HomepagePage.class);
        loginPage = PageFactory.initElements(browser, LoginPage.class);
        requestsPage = PageFactory.initElements(browser, RequestsPage.class);
        adminPage = PageFactory.initElements(browser, AdminPage.class);

        homepagePage.ensureIsDisplayedLogin();
        homepagePage.getLogin().click();

        loginPage.ensureIsDisplayedTitle();
        loginPage.loginAsAdmin();

        adminPage.ensureIsDisplayedProcessRequests();
        adminPage.getProcessRequests().click();


    }

    @Test
    private void Accept(){

        requestsPage.ensureIsDisplayedTitle();

        assertEquals("http://localhost:4200/process_requests", browser.getCurrentUrl());

        int noOfRequests = requestsPage.getRequestsTableSize();


        requestsPage.ensureIsDisplayedAccept();
        requestsPage.getAcceptButton().click();

        requestsPage.ensureIsDisplayedSuccess();

        requestsPage.ensureIsDeleted(noOfRequests);


    }

    @Test
    private void Deny(){

        requestsPage.ensureIsDisplayedTitle();

        assertEquals("http://localhost:4200/process_requests", browser.getCurrentUrl());

        int noOfRequests = requestsPage.getRequestsTableSize();


        requestsPage.ensureIsDisplayedDelete();
        requestsPage.getDeleteButton().click();

        requestsPage.ensureIsDisplayedSuccess();

        requestsPage.ensureIsDeleted(noOfRequests);

    }

    @Test
    private void NoRequests(){

        requestsPage.ensureIsDisplayedTitle();

        assertEquals("http://localhost:4200/process_requests", browser.getCurrentUrl());

        requestsPage.ensureIsDisplayedNoRequests();


    }

}
