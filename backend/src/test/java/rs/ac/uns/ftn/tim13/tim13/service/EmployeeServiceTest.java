package rs.ac.uns.ftn.tim13.tim13.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import rs.ac.uns.ftn.tim13.tim13.dto.EmployeeDTO;
import rs.ac.uns.ftn.tim13.tim13.dto.PassengerDTO;
import rs.ac.uns.ftn.tim13.tim13.exceptions.EmployeeNotFoundException;
import rs.ac.uns.ftn.tim13.tim13.exceptions.PassengerNotFoundException;
import rs.ac.uns.ftn.tim13.tim13.exceptions.UserFromTokenNotFoundException;
import rs.ac.uns.ftn.tim13.tim13.model.*;
import rs.ac.uns.ftn.tim13.tim13.repository.AccountRepository;
import rs.ac.uns.ftn.tim13.tim13.repository.EmployeeRepository;
import rs.ac.uns.ftn.tim13.tim13.repository.PassengerRepository;

import javax.validation.constraints.Email;
import java.util.*;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class EmployeeServiceTest {

    @Autowired
    EmployeeService employeeService;

    @Autowired
    AuthorityService authorityService;

    @Autowired
    EmailService emailService;

    @MockBean
    private AccountRepository accountRepository;

    @MockBean
    private PassengerRepository passengerRepository;

    @MockBean
    private EmployeeRepository employeeRepository;

    private EmployeeDTO employeeDTO;

    @Before
    public void setUp(){

        List<Passenger> verifiedPassengers = new List<Passenger>() {
            @Override
            public int size() {
                return 0;
            }

            @Override
            public boolean isEmpty() {
                return false;
            }

            @Override
            public boolean contains(Object o) {
                return false;
            }

            @Override
            public Iterator<Passenger> iterator() {
                return null;
            }

            @Override
            public Object[] toArray() {
                return new Object[0];
            }

            @Override
            public <T> T[] toArray(T[] a) {
                return null;
            }

            @Override
            public boolean add(Passenger passenger) {
                return false;
            }

            @Override
            public boolean remove(Object o) {
                return false;
            }

            @Override
            public boolean containsAll(Collection<?> c) {
                return false;
            }

            @Override
            public boolean addAll(Collection<? extends Passenger> c) {
                return false;
            }

            @Override
            public boolean addAll(int index, Collection<? extends Passenger> c) {
                return false;
            }

            @Override
            public boolean removeAll(Collection<?> c) {
                return false;
            }

            @Override
            public boolean retainAll(Collection<?> c) {
                return false;
            }

            @Override
            public void clear() {

            }

            @Override
            public Passenger get(int index) {
                return null;
            }

            @Override
            public Passenger set(int index, Passenger element) {
                return null;
            }

            @Override
            public void add(int index, Passenger element) {

            }

            @Override
            public Passenger remove(int index) {
                return null;
            }

            @Override
            public int indexOf(Object o) {
                return 0;
            }

            @Override
            public int lastIndexOf(Object o) {
                return 0;
            }

            @Override
            public ListIterator<Passenger> listIterator() {
                return null;
            }

            @Override
            public ListIterator<Passenger> listIterator(int index) {
                return null;
            }

            @Override
            public List<Passenger> subList(int fromIndex, int toIndex) {
                return null;
            }
        };

        Account admin = new Account("Mica", "123456", UserStatus.ACTIVATED);
        admin.setAuthority(authorityService.getByName("ADMIN"));
        Employee employee = new Employee("Milica", "Radojicic", "ksydfl@gmail.com", "KK Uzice", (long)60000000, new Date(2019, 01,24,19,42,11), (long)3);
        employee.setVerifiedPassengers(verifiedPassengers);
        admin.setEmployee(employee);
        employee.setAccount(admin);

        Mockito.when(accountRepository.findByUsername("Mica")).thenReturn(admin);

        Account passengerAcc = new Account("LALALALALALAL", "123456", UserStatus.PENDING);
        passengerAcc.setAuthority(authorityService.getByName("PASSENGER"));
        Passenger passenger = new Passenger("Ime", "Ime", "k@gmail.com", "isjdhfa", "PENSIONER", new Date(2020, 11, 01, 22, 41, 35), PassengerType.PENSIONER, null);
        passenger.setId((long)6);
        passengerAcc.setPassenger(passenger);
        passenger.setAccount(passengerAcc);

        Optional<Passenger> passangerO = Optional.of(passenger);

        Mockito.when(passengerRepository.findById((long)6)).thenReturn(passangerO);

       /* Mockito.when(emailService.sendMail("Your account has been verified!", "Dear" + "Ime" +
                "Your account has officially been verified and you can start using it. Best regards", "k@gmail.com")).thenReturn();
*/

        Account a = new Account("Lala1", "123456", UserStatus.valueOf("ACTIVATED"));
        Employee e = new Employee("Lala", "Lalovic", "katarina.radisic.k@gmail.com", "LlALALALA",
                (long)1234567876, new Date(2019, 01, 17, 22, 22, 32), (long)12351);
        a.setEmployee(e);
        e.setAccount(a);

        Account a1 = new Account("XYZNML", "123456", UserStatus.valueOf("ACTIVATED"));
        Employee e1 = new Employee("Lala", "Lalovic", "katarina.radisic.k@gmail.com", "LlALALALA",
                (long)1234567876, new Date(2019, 01, 17, 22, 22, 32), (long)12351);
        a.setEmployee(e);
        e.setAccount(a);

        Mockito.when(accountRepository.findByUsername("XYZNML")).thenReturn(null);
        Mockito.when(accountRepository.findByUsername("Lala1")).thenReturn(a);

       this.employeeDTO = new EmployeeDTO("Milorad", "Mimic", "ksydfl@gmail.com", "Mimina Adresa", (long) 123456, "XYZNML", "12343", "EMPLOYEE");

       Mockito.when(employeeRepository.save(e1)).thenReturn(e1);


    }

    @Test
    public void newEmployee(){

        Employee employee = employeeService.newEmployee(this.employeeDTO);

        assertNotNull(employee);

    }

    @Test
    public void newEmployeeEXISTS(){

        EmployeeDTO dto = new EmployeeDTO("Milorad", "Mimic", "ksydfl@gmail.com", "Mimina Adresa", (long) 123456, "Lala1", "12343", "EMPLOYEE");

        Employee employee = employeeService.newEmployee(dto);

        assertNull(employee);

    }


    @Test
    public void getAllNOT_SUCCESSFUL(){

        List<PassengerDTO> dtos = employeeService.getAllUnverified();

        boolean b = true;

        System.out.println("VELICINA LISTE");
        System.out.println(dtos.size());

        if(dtos.size() == 0) b = false;

        assertFalse(b);


    }

   @Test
    public void getAll(){

        List<PassengerDTO> dtos = employeeService.getAllUnverified();

        boolean b = true;

        if(dtos.size() == 0) b = false;

        assertTrue(b);


    }

    @Test
    public void accept() throws UserFromTokenNotFoundException, EmployeeNotFoundException, PassengerNotFoundException {

        Passenger passenger = employeeService.verifyPassenger("eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJNaWNhIiwiY3JlYXRlZCI6MTU0ODcxMTgwODQwMiwicm9sZXMiOlsiQURNSU4iXSwiZXhwIjoxNTQ4NzI5ODA4fQ.4WhldYDOnrPuoXdfsIbcodlSE3eUyck_vu4JC5B61FBQ_pVP-K6F-5dmwtd7PGcUyHh3cN4daAwTojAQ3Yx_Zg",
                (long) 6);

        assertNotNull(passenger);




    }

    @Test(expected = UserFromTokenNotFoundException.class)
    public void acceptUserNotFound() throws UserFromTokenNotFoundException, EmployeeNotFoundException, PassengerNotFoundException{

        Passenger passenger = employeeService.verifyPassenger("ciOiJIUzUxMiJ9.eyJzdWIiOiJNaWNhIiwiY3JlYXRlZCI6MTU0ODcxMTgwODQwMiwicm9sZXMiOlsiQURNSU4iXSwiZXhwIjoxNTQ4NzI5ODA4fQ.4WhldYDOnrPuoXdfsIbcodlSE3eUyck_vu4JC5B61FBQ_pVP-K6F-5dmwtd7PGcUyHh3cN4daAwTojAQ3Yx_Zg",
                (long) 6);

    }

    @Test(expected = PassengerNotFoundException.class)
    public void acceptPassengerNotFound() throws UserFromTokenNotFoundException, EmployeeNotFoundException, PassengerNotFoundException {

        Passenger passenger = employeeService.verifyPassenger("eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJNaWNhIiwiY3JlYXRlZCI6MTU0ODcxMTgwODQwMiwicm9sZXMiOlsiQURNSU4iXSwiZXhwIjoxNTQ4NzI5ODA4fQ.4WhldYDOnrPuoXdfsIbcodlSE3eUyck_vu4JC5B61FBQ_pVP-K6F-5dmwtd7PGcUyHh3cN4daAwTojAQ3Yx_Zg",
                (long) 2345678);

    }

    @Test(expected = PassengerNotFoundException.class)
    public void denyPassengerNotFound() throws PassengerNotFoundException {

        boolean b = employeeService.denyPassenger((long)234);

    }











}
