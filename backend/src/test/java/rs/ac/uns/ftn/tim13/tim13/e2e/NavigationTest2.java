package rs.ac.uns.ftn.tim13.tim13.e2e;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import rs.ac.uns.ftn.tim13.tim13.pages.*;

import static org.testng.AssertJUnit.assertEquals;

public class NavigationTest2 {

    private WebDriver browser;

    private HomepagePage homepagePage;

    private LoginPage loginPage;

    private EmployeePage employeePage;

    private AdminPage adminPage;

    private PassengerPage passengerPage;

    private RequestsPage requestsPage;

    private NewEmployeePage newEmployeePage;

    @BeforeMethod
    public void setupSelenium(){

        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Windows User\\Desktop\\chromedriver.exe");
        browser = new ChromeDriver();

        browser.manage().window().maximize();

        browser.navigate().to("http://localhost:4200/homepage");

        homepagePage = PageFactory.initElements(browser, HomepagePage.class);
        loginPage = PageFactory.initElements(browser, LoginPage.class);
        employeePage = PageFactory.initElements(browser, EmployeePage.class);
        adminPage = PageFactory.initElements(browser, AdminPage.class);
        passengerPage = PageFactory.initElements(browser, PassengerPage.class);
        requestsPage = PageFactory.initElements(browser, RequestsPage.class);
        newEmployeePage = PageFactory.initElements(browser, NewEmployeePage.class);

        homepagePage.ensureIsDisplayedLogin();
        homepagePage.getLogin().click();

    }


    @Test
    public void navigateEmployeePage(){

        assertEquals("http://localhost:4200/login", browser.getCurrentUrl());

        homepagePage.getLogin().click();

        loginPage.ensureIsDisplayedTitle();

        loginPage.loginAsEmployee();

        employeePage.ensureIsDisplayedProcessRequests();

        assertEquals("http://localhost:4200/employee_homepage", browser.getCurrentUrl());

    }

    @Test
    public void navigateAdminPage(){

        assertEquals("http://localhost:4200/login", browser.getCurrentUrl());

        loginPage.ensureIsDisplayedTitle();

        loginPage.loginAsAdmin();

        adminPage.ensureIsDisplayedProcessRequests();

        assertEquals("http://localhost:4200/admin_homepage", browser.getCurrentUrl());

    }

    @Test
    public void navigatePassengerPage(){

        assertEquals("http://localhost:4200/login", browser.getCurrentUrl());

        loginPage.ensureIsDisplayedTitle();

        loginPage.loginAsPassenger();

        passengerPage.ensureIsDisplayedWelcome();

        assertEquals("http://localhost:4200/passenger_homepage", browser.getCurrentUrl());

    }

    @Test
    public void navigateEmployeeProcessRequests(){

        assertEquals("http://localhost:4200/login", browser.getCurrentUrl());

        loginPage.ensureIsDisplayedTitle();

        loginPage.loginAsEmployee();

        employeePage.ensureIsDisplayedProcessRequests();

        employeePage.getProcessRequests().click();

        requestsPage.ensureIsDisplayedTitle();

        assertEquals("http://localhost:4200/process_requests", browser.getCurrentUrl());

    }

    @Test
    public void navigateAdminProcessRequests(){

        assertEquals("http://localhost:4200/login", browser.getCurrentUrl());

        loginPage.ensureIsDisplayedTitle();

        loginPage.loginAsAdmin();

        adminPage.ensureIsDisplayedProcessRequests();

        adminPage.getProcessRequests().click();

        requestsPage.ensureIsDisplayedTitle();

        assertEquals("http://localhost:4200/process_requests", browser.getCurrentUrl());

    }

    @Test
    public void navigateNewEmployee(){

        assertEquals("http://localhost:4200/login", browser.getCurrentUrl());

        loginPage.ensureIsDisplayedTitle();

        loginPage.loginAsAdmin();

        adminPage.ensureIsDisplayedAddEmployee();

        adminPage.getAddEmployee().click();

        newEmployeePage.ensureIsDisplayeTitle();

        assertEquals("http://localhost:4200/new_employee", browser.getCurrentUrl());

    }

}
