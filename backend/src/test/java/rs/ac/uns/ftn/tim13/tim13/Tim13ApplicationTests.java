package rs.ac.uns.ftn.tim13.tim13;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class Tim13ApplicationTests {

	@Test
	public void contextLoads() {
	}

}
