package rs.ac.uns.ftn.tim13.tim13.repository;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.testng.annotations.Test;
import rs.ac.uns.ftn.tim13.tim13.model.Account;
import rs.ac.uns.ftn.tim13.tim13.model.Authority;
import rs.ac.uns.ftn.tim13.tim13.model.Employee;
import rs.ac.uns.ftn.tim13.tim13.model.UserStatus;
import rs.ac.uns.ftn.tim13.tim13.service.AuthorityService;

import java.util.Date;

import static org.testng.AssertJUnit.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
public class EmployeeRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private EmployeeRepository employeeRepository;

    @MockBean
    private AuthorityService authorityService;

    private String username;

    @Before
    public void setUp(){

        this.username = "RepTest";

        Authority authority = new Authority((long) 4, "EMPLOYEE");

        Mockito.when(authorityService.getByName("EMPLOYEE")).thenReturn(authority);

        Account a = new Account(this.username, "$10$L3Vb0zKgGwignJZXslM97.SvpjL9W6KpOLGv/78FBxGSqYxqFgbGK",
                UserStatus.ACTIVATED);

        a.setAuthority(authorityService.getByName("EMPLOYEE"));

        entityManager.persist(a);
        entityManager.flush();


        Employee e = new Employee((long)666, "RepTest", "RepTest", "RepTest@gmail.com",
                "RepTest", (long) 1234, new Date(2019, 1, 11, 17, 20) ,(long) 1234);

        e.setAccount(a);

        entityManager.persist(e);
        entityManager.flush();




        a.setEmployee(e);



        entityManager.persist(a);
        entityManager.flush();



    }

    @Test
    public void whenFindByUsername_thenReturnEmployee(){


        Employee found = employeeRepository.findByUsername(this.username);

        assertEquals(this.username, found.getAccount().getUsername());

    }

}
