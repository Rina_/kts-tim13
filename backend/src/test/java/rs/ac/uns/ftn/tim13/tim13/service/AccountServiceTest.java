package rs.ac.uns.ftn.tim13.tim13.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.junit4.SpringRunner;
import rs.ac.uns.ftn.tim13.tim13.dto.LoginDTO;
import rs.ac.uns.ftn.tim13.tim13.exceptions.AccountNotFoundException;
import rs.ac.uns.ftn.tim13.tim13.exceptions.UsernameExistsException;
import rs.ac.uns.ftn.tim13.tim13.model.Account;
import rs.ac.uns.ftn.tim13.tim13.model.Authority;
import rs.ac.uns.ftn.tim13.tim13.model.Employee;
import rs.ac.uns.ftn.tim13.tim13.model.UserStatus;
import rs.ac.uns.ftn.tim13.tim13.repository.AccountRepository;
import rs.ac.uns.ftn.tim13.tim13.repository.AuthorityRepository;

import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AccountServiceTest {

    @Autowired
    private AccountService accountService;

    @Autowired
    private AuthorityRepository authorityRepository;

    @MockBean
    private AccountRepository accountRepository;

    @Before
    public void setUp(){



        Account a = new Account("Lala1", "123456", UserStatus.valueOf("ACTIVATED"));
        Employee e = new Employee("Lala", "Lalovic", "katarina.radisic.k@gmail.com", "LlALALALA",
                (long)1234567876, new Date(2019, 01, 17, 22, 22, 32), (long)12351);
        a.setEmployee(e);
        e.setAccount(a);

        Mockito.when(accountRepository.findByUsername("Micija")).thenReturn(null);
        Mockito.when(accountRepository.findByUsername("Mima")).thenReturn(null);
        Mockito.when(accountRepository.findByUsername("Lala1")).thenReturn(a);
    }

    @Test(expected = AccountNotFoundException.class)
    public void login_usernameNotFound(){
        LoginDTO dto = new LoginDTO("NANANA", "123456");
        accountService.login(dto);
    }

    @Test
    public void login(){
        LoginDTO dto = new LoginDTO("Lala1", "123456");
        accountService.login(dto);
    }

    @Test
    public void register() throws UsernameExistsException {
        Account acc = new Account("Korisnik", "123456");

        accountService.register(acc);
    }

    @Test(expected = UsernameExistsException.class)
    public void registerFAIL() throws UsernameExistsException{

        Account acc = new Account("Lala1", "123456");

        accountService.register(acc);

    }

}
