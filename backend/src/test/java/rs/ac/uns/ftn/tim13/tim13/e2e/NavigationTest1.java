package rs.ac.uns.ftn.tim13.tim13.e2e;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import rs.ac.uns.ftn.tim13.tim13.pages.*;

import static org.testng.AssertJUnit.assertEquals;

public class NavigationTest1 {

    private WebDriver browser;

    private HomepagePage homepagePage;

    private LoginPage loginPage;

    private RegisterPage registerPage;

    @BeforeMethod
    public void setupSelenium(){

        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Windows User\\Desktop\\chromedriver.exe");
        browser = new ChromeDriver();

        browser.manage().window().maximize();

        browser.navigate().to("http://localhost:4200/homepage");

        homepagePage = PageFactory.initElements(browser, HomepagePage.class);
        loginPage = PageFactory.initElements(browser, LoginPage.class);
        registerPage = PageFactory.initElements(browser, RegisterPage.class);

    }

    @Test
    public void navigateLogin(){

        homepagePage.ensureIsDisplayedLogin();

        homepagePage.getLogin().click();

        loginPage.ensureIsDisplayedTitle();

        assertEquals("http://localhost:4200/login", browser.getCurrentUrl());

    }

    @Test
    public void navigateRegister(){

        homepagePage.ensureIsDisplayedRegister();

        homepagePage.getRegister().click();

        registerPage.ensureIsDisplayedUsername();

        assertEquals("http://localhost:4200/register", browser.getCurrentUrl());

    }



}
