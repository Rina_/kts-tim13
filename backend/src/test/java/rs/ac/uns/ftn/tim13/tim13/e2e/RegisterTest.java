package rs.ac.uns.ftn.tim13.tim13.e2e;

import org.h2.store.Page;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import rs.ac.uns.ftn.tim13.tim13.pages.*;
import sun.rmi.runtime.Log;

import static org.testng.AssertJUnit.assertEquals;

public class RegisterTest {

    private WebDriver browser;

    private HomepagePage homepagePage;

    private RegisterPage registerPage;

    private LoginPage loginPage;


    @BeforeMethod
    public void setupSelenium(){

        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Windows User\\Desktop\\chromedriver.exe");
        browser = new ChromeDriver();

        browser.manage().window().maximize();

        browser.navigate().to("http://localhost:4200/homepage");

        homepagePage = PageFactory.initElements(browser, HomepagePage.class);
        registerPage = PageFactory.initElements(browser, RegisterPage.class);
        loginPage = PageFactory.initElements(browser, LoginPage.class);

        homepagePage.getRegister().click();

    }

    @Test
    public void RegisterSuccess(){

        assertEquals("http://localhost:4200/register", browser.getCurrentUrl());

        registerPage.ensureIsDisplayedFirstName();
        registerPage.setFirstName("Glupost");

        registerPage.ensureIsDisplayedLastName();
        registerPage.setLastName("Glupost");

        registerPage.ensureIsDisplayedUsername();
        registerPage.setUsername("Test1");

        registerPage.ensureIsDisplayedPassword();
        registerPage.setPassword("123456");

        registerPage.ensureIsDisplayedPasswordConfirmation();
        registerPage.setConfirmPassword("123456");

        registerPage.ensureIsDisplayedEmail();
        registerPage.setEmail("ksydfl@gmail.com");

        registerPage.ensureIsDisplayedAddress();
        registerPage.setAddress("Adresa 1");

        registerPage.ensureIsDisplayedPassengerType();
        registerPage.getPassengerType().click();

        registerPage.ensureIsDisplayedPensioner();
        registerPage.getPensioner().click();

        registerPage.ensureIsDisplayedConfirmation();
        registerPage.setConfirmation("PENSIONER");

        registerPage.ensureIsDisplayedRegister();
        registerPage.getRegister().click();

        loginPage.ensureIsDisplayedTitle();

        assertEquals("http://localhost:4200/login", browser.getCurrentUrl());
    }

    @Test
    public void RegisterUsernameExists(){

        assertEquals("http://localhost:4200/register", browser.getCurrentUrl());

        registerPage.ensureIsDisplayedFirstName();
        registerPage.setFirstName("Glupost");

        registerPage.ensureIsDisplayedLastName();
        registerPage.setLastName("Glupost");

        registerPage.ensureIsDisplayedUsername();
        registerPage.setUsername("Lala1");

        registerPage.ensureIsDisplayedPassword();
        registerPage.setPassword("123456");

        registerPage.ensureIsDisplayedPasswordConfirmation();
        registerPage.setConfirmPassword("123456");

        registerPage.ensureIsDisplayedEmail();
        registerPage.setEmail("ksydfl@gmail.com");

        registerPage.ensureIsDisplayedAddress();
        registerPage.setAddress("Adresa 1");

        registerPage.ensureIsDisplayedPassengerType();
        registerPage.getPassengerType().click();

        registerPage.ensureIsDisplayedPensioner();
        registerPage.getPensioner().click();

        registerPage.ensureIsDisplayedConfirmation();
        registerPage.setConfirmation("PENSIONER");

        registerPage.ensureIsDisplayedRegister();
        registerPage.getRegister().click();

        registerPage.ensureIsDisplayedUsernameExists();
    }

    @Test
    public void FirstNameRequired(){

        assertEquals("http://localhost:4200/register", browser.getCurrentUrl());

        registerPage.ensureIsDisplayedFirstName();
        registerPage.setFirstName("");

        registerPage.getBackground().click();


        registerPage.ensureIsDisplayedFirstNameRequired();

    }

    @Test
    public void LastNameRequired(){

        assertEquals("http://localhost:4200/register", browser.getCurrentUrl());

        registerPage.ensureIsDisplayedLastName();
        registerPage.setLastName("");

        registerPage.getBackground().click();


        registerPage.ensureIsDisplayedLastNameRequired();

    }

    @Test
    public void UsernameRequired(){

        assertEquals("http://localhost:4200/register", browser.getCurrentUrl());

        registerPage.ensureIsDisplayedUsername();
        registerPage.setUsername("");

        registerPage.getBackground().click();


        registerPage.ensureIsDisplayedUsernameRequired();

    }

    @Test
    public void PasswordRequired(){

        assertEquals("http://localhost:4200/register", browser.getCurrentUrl());

        registerPage.ensureIsDisplayedPassword();
        registerPage.setPassword("");

        registerPage.getBackground().click();


        registerPage.ensureIsDisplayedPasswordRequired();

    }

    @Test
    public void PasswordConfirmationRequired(){

        assertEquals("http://localhost:4200/register", browser.getCurrentUrl());

        registerPage.ensureIsDisplayedPasswordConfirmation();
        registerPage.setConfirmPassword("");

        registerPage.getBackground().click();


        registerPage.ensureIsDisplayedPasswordConfirmationRequired();

    }

    @Test
    public void EmailRequired(){

        assertEquals("http://localhost:4200/register", browser.getCurrentUrl());

        registerPage.ensureIsDisplayedEmail();
        registerPage.setEmail("");

        registerPage.getBackground().click();


        registerPage.ensureIsDisplayedEmailRequired();

    }

    @Test
    public void AddressRequired(){

        assertEquals("http://localhost:4200/register", browser.getCurrentUrl());

        registerPage.ensureIsDisplayedAddress();
        registerPage.setAddress("");

        registerPage.getBackground().click();


        registerPage.ensureIsDisplayedAddressRequired();

    }

    @Test
    public void PasswordsDontMatch(){

        assertEquals("http://localhost:4200/register", browser.getCurrentUrl());

        registerPage.ensureIsDisplayedPassword();
        registerPage.setPassword("123456");

        registerPage.ensureIsDisplayedPasswordConfirmation();
        registerPage.setConfirmPassword("654321");

        registerPage.getBackground().click();


        registerPage.ensureIsDisplayedPasswordsDontMatch();

    }

    @Test
    public void InvalidEmail(){

        assertEquals("http://localhost:4200/register", browser.getCurrentUrl());

        registerPage.ensureIsDisplayedEmail();
        registerPage.setEmail("mejl");

        registerPage.getBackground().click();


        registerPage.ensureIsDisplayedIvalidEmail();

    }

    @Test
    public void backToLogin(){

        assertEquals("http://localhost:4200/register", browser.getCurrentUrl());

        registerPage.ensureIsDisplayedLogin();

        registerPage.getLogin().click();

        loginPage.ensureIsDisplayedTitle();

        assertEquals("http://localhost:4200/login", browser.getCurrentUrl());


    }



}
