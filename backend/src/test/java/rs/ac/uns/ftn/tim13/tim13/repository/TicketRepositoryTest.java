package rs.ac.uns.ftn.tim13.tim13.repository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import rs.ac.uns.ftn.tim13.tim13.model.Account;
import rs.ac.uns.ftn.tim13.tim13.model.Passenger;
import rs.ac.uns.ftn.tim13.tim13.model.Ticket;

import javax.validation.constraints.AssertFalse;
import java.util.ArrayList;
import java.util.Optional;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
public class TicketRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    TicketRepository ticketRepository;

    @Before
    public void setUp(){
        Ticket t1 = new Ticket();
        Passenger p1= new Passenger();
        Account a1 = new Account();
        a1.setUsername("mihajlo");
        Account a2 = new Account();
        a1.setUsername("kac");
        p1.setAccount(a1);
        Ticket t2 = new Ticket();
        Passenger p2= new Passenger();
        p2.setAccount(a2);
        t1.setId(1L);
        t1.setPassenger(p1);
        t2.setId(2L);
        t2.setPassenger(p2);
        entityManager.persist(t1);
        entityManager.persist(t2);
    }

    @Test
    public void testfindbyId(){
        Optional<Ticket> t = ticketRepository.findbyId(1L);

        assertEquals("mihajlo",t.get().getPassenger().getAccount().getUsername());

    }
    @Test
    public void testfindbyUsername(){
        ArrayList<Ticket> tickets = ticketRepository.findbyUsername("mihajlo");

        assertEquals(1, tickets.size());

    }

}
