package rs.ac.uns.ftn.tim13.tim13.e2e;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import rs.ac.uns.ftn.tim13.tim13.pages.*;

import static org.testng.AssertJUnit.assertEquals;

public class NewEmployeeTest {


    private WebDriver browser;

    private HomepagePage homepagePage;

    private LoginPage loginPage;

    private AdminPage adminPage;

    private NewEmployeePage newEmployeePage;

    @BeforeMethod
    public void setupSelenium(){

        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Windows User\\Desktop\\chromedriver.exe");
        browser = new ChromeDriver();

        browser.manage().window().maximize();

        browser.navigate().to("http://localhost:4200/homepage");

        homepagePage = PageFactory.initElements(browser, HomepagePage.class);
        loginPage = PageFactory.initElements(browser, LoginPage.class);
        adminPage = PageFactory.initElements(browser, AdminPage.class);
        newEmployeePage = PageFactory.initElements(browser, NewEmployeePage.class);

        homepagePage.getLogin().click();
        loginPage.loginAsAdmin();

        adminPage.ensureIsDisplayedAddEmployee();
        adminPage.getAddEmployee().click();

    }

    @Test
    public void newEmployee(){

        newEmployeePage.ensureIsDisplayeTitle();

        assertEquals("http://localhost:4200/new_employee", browser.getCurrentUrl());

        newEmployeePage.ensureIsDisplayedFirstName();
        newEmployeePage.setFirstName("Glupost");

        newEmployeePage.ensureIsDisplayedLastName();
        newEmployeePage.setLastName("Glupost");

        newEmployeePage.ensureIsDisplayedUsername();
        newEmployeePage.setUsername("Glupost");

        newEmployeePage.ensureIsDisplayedPassword();
        newEmployeePage.setPassword("123456");

        newEmployeePage.ensureIsDisplayedPasswordConfirmation();
        newEmployeePage.setPasswordConfirmation("123456");

        newEmployeePage.ensureIsDisplayedEmail();
        newEmployeePage.setEmail("ksydfl@gmail.com");

        newEmployeePage.ensureIsDisplayedAddress();
        newEmployeePage.setAddress("Adresa1");

        newEmployeePage.ensureIsDisplayedPhoneNumber();
        newEmployeePage.setPhoneNumber("123456789");

        newEmployeePage.ensureIsDisplayedAuthority();
        newEmployeePage.getAdmin().click();

        newEmployeePage.ensureIsDisplayedRegister();
        newEmployeePage.getRegister().click();

        adminPage.ensureIsDisplayedAddEmployee();

        assertEquals("http://localhost:4200/admin_homepage", browser.getCurrentUrl());

    }

    @Test
    public void usernameExists(){

        newEmployeePage.ensureIsDisplayeTitle();

        assertEquals("http://localhost:4200/new_employee", browser.getCurrentUrl());

        newEmployeePage.ensureIsDisplayedFirstName();
        newEmployeePage.setFirstName("Glupost");

        newEmployeePage.ensureIsDisplayedLastName();
        newEmployeePage.setLastName("Glupost");

        newEmployeePage.ensureIsDisplayedUsername();
        newEmployeePage.setUsername("Lala1");

        newEmployeePage.ensureIsDisplayedPassword();
        newEmployeePage.setPassword("123456");

        newEmployeePage.ensureIsDisplayedPasswordConfirmation();
        newEmployeePage.setPasswordConfirmation("123456");

        newEmployeePage.ensureIsDisplayedEmail();
        newEmployeePage.setEmail("ksydfl@gmail.com");

        newEmployeePage.ensureIsDisplayedAddress();
        newEmployeePage.setAddress("Adresa1");

        newEmployeePage.ensureIsDisplayedPhoneNumber();
        newEmployeePage.setPhoneNumber("123456789");

        newEmployeePage.ensureIsDisplayedAuthority();
        newEmployeePage.getAdmin().click();

        newEmployeePage.ensureIsDisplayedRegister();
        newEmployeePage.getRegister().click();

        newEmployeePage.ensureIsDisplayedUsernameExists();

    }

    @Test
    public void FirstNameRequired(){

        newEmployeePage.ensureIsDisplayeTitle();

        assertEquals("http://localhost:4200/new_employee", browser.getCurrentUrl());

        newEmployeePage.ensureIsDisplayedFirstName();
        newEmployeePage.setFirstName("");

        newEmployeePage.getBackground().click();

        newEmployeePage.ensureIsDisplayedFirstNameRequired();

    }

    @Test
    public void LastNameRequired(){

        newEmployeePage.ensureIsDisplayeTitle();

        assertEquals("http://localhost:4200/new_employee", browser.getCurrentUrl());

        newEmployeePage.ensureIsDisplayedLastName();
        newEmployeePage.setLastName("");

        newEmployeePage.getBackground().click();

        newEmployeePage.ensureIsDisplayedLastNameRequired();
    }

    @Test
    public void UsernameRequired(){

        newEmployeePage.ensureIsDisplayeTitle();

        assertEquals("http://localhost:4200/new_employee", browser.getCurrentUrl());

        newEmployeePage.ensureIsDisplayedUsername();
        newEmployeePage.setUsername("");

        newEmployeePage.getBackground().click();

        newEmployeePage.ensureIsDisplayedUsernameRequired();

    }

    @Test
    public void PasswordRequired(){

        newEmployeePage.ensureIsDisplayeTitle();

        assertEquals("http://localhost:4200/new_employee", browser.getCurrentUrl());

        newEmployeePage.ensureIsDisplayedPassword();
        newEmployeePage.setPassword("");

        newEmployeePage.getBackground().click();

        newEmployeePage.ensureIsDisplayedPasswordRequired();

    }

    @Test
    public void PasswordConfirmationRequired(){

        newEmployeePage.ensureIsDisplayeTitle();

        assertEquals("http://localhost:4200/new_employee", browser.getCurrentUrl());

        newEmployeePage.ensureIsDisplayedPasswordConfirmation();
        newEmployeePage.setPasswordConfirmation("");

        newEmployeePage.getBackground().click();

        newEmployeePage.ensureIsDisplayedPasswordConfirmationRequired();

    }

    @Test
    public void EmailRequired(){

        newEmployeePage.ensureIsDisplayeTitle();

        assertEquals("http://localhost:4200/new_employee", browser.getCurrentUrl());

        newEmployeePage.ensureIsDisplayedEmail();
        newEmployeePage.setEmail("");

        newEmployeePage.getBackground().click();

        newEmployeePage.ensureIsDisplayedEmailRequired();

    }

    @Test
    public void AddressRequired(){

        newEmployeePage.ensureIsDisplayeTitle();

        assertEquals("http://localhost:4200/new_employee", browser.getCurrentUrl());

        newEmployeePage.ensureIsDisplayedAddress();
        newEmployeePage.setAddress("");

        newEmployeePage.getBackground().click();

        newEmployeePage.ensureIsDisplayedAddressRequired();

    }

    @Test
    public void PhoneNumberRequired(){

        newEmployeePage.ensureIsDisplayeTitle();

        assertEquals("http://localhost:4200/new_employee", browser.getCurrentUrl());

        newEmployeePage.ensureIsDisplayedPhoneNumber();
        newEmployeePage.setPhoneNumber("");

        newEmployeePage.getBackground().click();

        newEmployeePage.ensureIsDisplayedPhoneNumberRequired();

    }

    @Test
    public void PasswordsDontMatch(){

        newEmployeePage.ensureIsDisplayeTitle();

        assertEquals("http://localhost:4200/new_employee", browser.getCurrentUrl());

        newEmployeePage.ensureIsDisplayedPassword();
        newEmployeePage.setPassword("123456");

        newEmployeePage.ensureIsDisplayedPasswordConfirmation();
        newEmployeePage.setPasswordConfirmation("654321");

        newEmployeePage.getBackground().click();

        newEmployeePage.ensureIsDisplayedPasswordsDontMatch();

    }

    @Test
    public void InvalidEmail(){

        newEmployeePage.ensureIsDisplayeTitle();

        assertEquals("http://localhost:4200/new_employee", browser.getCurrentUrl());

        newEmployeePage.ensureIsDisplayedEmail();
        newEmployeePage.setEmail("Email");

        newEmployeePage.getBackground().click();

        newEmployeePage.ensureIsDisplayedInvalidEmail();

    }




}
