package rs.ac.uns.ftn.tim13.tim13.controllers;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import rs.ac.uns.ftn.tim13.tim13.dto.TicketDTO;
import rs.ac.uns.ftn.tim13.tim13.model.Ticket;
import rs.ac.uns.ftn.tim13.tim13.service.TicketService;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TicketControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private TicketService ticketService;

    @Test
    public void testCreateTicket(){

        ResponseEntity<TicketDTO> responseEntity= restTemplate.postForEntity("/api/tickets", new TicketDTO("m", "1", "1"), TicketDTO.class);

        TicketDTO ticket= responseEntity.getBody();

        assertNotNull(ticket);
        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        assertEquals("m", ticket.getUser());
        assertEquals("1", ticket.getTicketType());
        assertEquals("1", ticket.getTransportType());




    }

}

