import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Employee} from "../../models/employee";

@Injectable()
export class EmployeeService {

  constructor(private http: HttpClient) {  }

  public acceptRequest(id){
    return this.http.patch('api/accept_request?id='+ id, null);
  }

  public getAll(){
    return this.http.get('api/get_all');
  }

  public denyRequest(id){
    return this.http.get('api/deny_request?id='+ id);
  }

  public newEmployee(employee: Employee){
    return this.http.post('api/new_employee', employee)
  }

}
