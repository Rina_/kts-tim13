import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Passenger} from '../../models/passenger';

@Injectable()
export class UserService {

  constructor(private http: HttpClient) { }

  public register(passenger: Passenger){
    return this.http.post('http://localhost:8080/api/register', passenger)
  }

}
