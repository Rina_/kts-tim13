import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {PasswordValidator} from '../../shared/validators/password.validatior';
import {SpaceValidator} from "../../shared/validators/space.validator";
import {Employee} from "../../models/employee";
import {EmployeeService} from "../../services/employee/employee.service";
import {Router} from "@angular/router";
import {ToasterService} from "angular5-toaster/dist";

@Component({
  selector: 'app-new-employee',
  templateUrl: './new-employee.component.html',
  styleUrls: ['./new-employee.component.css'],
  providers: [PasswordValidator]
})
export class NewEmployeeComponent implements OnInit {
  form: FormGroup;
  authority : string;
  list = ["ADMIN", "EMPLOYEE"];

  constructor(private fb: FormBuilder, private router : Router,
              private passwordValidator: PasswordValidator, private employeeService: EmployeeService,
              private toasterService : ToasterService) {

    this.form = this.fb.group({

      firstName: ['', [
        Validators.required,
        Validators.minLength(2)
      ]],
      lastName: ['', [
        Validators.required,
        Validators.minLength(2)
      ]],
      username: ['', [
        Validators.required,
        SpaceValidator.cannotContainSpace
      ]],
      passwords: fb.group({
        password: ['', [
          Validators.required,
          Validators.minLength(5)
        ]],
        confirmPassword: ['',
          Validators.required
        ]
      }, {validator: this.passwordValidator.matchPasswords.bind(this.passwordValidator)}),
      email: ['', [
        Validators.required, Validators.email
      ]],
      address: ['',[
        Validators.required,
        Validators.minLength(5)
      ]],
      phoneNumber:['',[
        Validators.required
      ]],

    })
  }

  get firstName() {
    return this.form.get('firstName');
  }

  get lastName() {
    return this.form.get('lastName');
  }

  get username() {
    return this.form.get('username');
  }

  get passwords() {
    return this.form.get('passwords');
  }

  get password() {
    return this.form.get('passwords').get('password');
  }

  get confirmPassword() {
    return this.form.get('passwords').get('confirmPassword');
  }

  get email() {
    return this.form.get('email');
  }

  get address(){
    return this.form.get('address');
  }

  get phoneNumber(){
    return this.form.get('phoneNumber');
  }


  ngOnInit() {
  }

  newEmployee(){

    let employee = new Employee(3, this.firstName.value, this.lastName.value, this.email.value, this.address.value,
      this.phoneNumber.value, null, this.username.value, this.password.value, this.authority);
    console.log(employee);

    this.employeeService.newEmployee(employee)
      .subscribe(()=> {
        this.router.navigate(['/admin_homepage']);
      }, error => {
        console.log(error);
        this.toasterService.pop('error', 'Error', 'Username already exists!');
      })

  }

}
