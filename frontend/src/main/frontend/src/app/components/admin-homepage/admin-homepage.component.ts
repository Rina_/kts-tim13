import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-admin-homepage',
  templateUrl: './admin-homepage.component.html',
  styleUrls: ['./admin-homepage.component.css']
})
export class AdminHomepageComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  newEmployee(){
    this.router.navigateByUrl('/new_employee');
  }

  processRequests(){
    this.router.navigateByUrl('/process_requests');
  }

}
