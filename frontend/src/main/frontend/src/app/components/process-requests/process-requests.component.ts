import { Component, OnInit } from '@angular/core';
import {Passenger} from "../../models/passenger";
import {ToasterConfig, ToasterService} from "angular5-toaster/dist";
import {EmployeeService} from "../../services/employee/employee.service";
import {JwtService} from "../../core/services/jwt.service";

@Component({
  selector: 'app-process-requests',
  templateUrl: './process-requests.component.html',
  styleUrls: ['./process-requests.component.css']
})
export class ProcessRequestsComponent implements OnInit {

  passengers : Array<Passenger>;
  toasterConfig: ToasterConfig;

  constructor(private employeeService: EmployeeService, jwtUtils: JwtService, private toasterService: ToasterService) {
    this.toasterConfig = new ToasterConfig({timeout: 3000});
    console.log(jwtUtils.decodeToken());
    this.employeeService.getAll().subscribe(data =>{
      console.log(data);
      this.passengers = data as Array<Passenger>;
    });


  }

  ngOnInit() {
  }

  Deny(passenger){
    this.employeeService.denyRequest(passenger.id).subscribe(data => {
      let index = this.passengers.indexOf(passenger);
      this.passengers.splice(index, 1);
    });
    this.toasterService.pop('success', 'Success!','You have successfully deleted the request!');
  }

  Accept(passenger) {
    this.employeeService.acceptRequest(passenger.id).subscribe(data =>{
      let index = this.passengers.indexOf(passenger);
      this.passengers.splice(index, 1);
    });
    this.toasterService.pop('success', 'Success!','You have successfully accepted the request!');
  }

}
