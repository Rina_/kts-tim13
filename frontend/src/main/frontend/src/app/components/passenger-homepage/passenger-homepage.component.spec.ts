import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PassengerHomepageComponent } from './passenger-homepage.component';

describe('PassengerHomepageComponent', () => {
  let component: PassengerHomepageComponent;
  let fixture: ComponentFixture<PassengerHomepageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PassengerHomepageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PassengerHomepageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
