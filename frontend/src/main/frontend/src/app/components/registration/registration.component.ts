import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {PasswordValidator} from "../../shared/validators/password.validatior";
import {SpaceValidator} from "../../shared/validators/space.validator";
import {Passenger} from "../../models/passenger";
import {UserService} from "../../services/user/user.service"
import {Router} from "@angular/router";
import {ConflictError} from "../../shared/errors/conflict-error";
import {ToasterConfig, ToasterService} from "angular5-toaster/dist";


@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css'],
  providers: [PasswordValidator, SpaceValidator]
})
export class RegistrationComponent implements OnInit {
  form: FormGroup;
  passengerType : string;
  list = ["Pensioner", "Student", "Unemployed", "Employed"];
  toasterConfig: ToasterConfig;

  constructor(private fb: FormBuilder,
              private passwordValidator: PasswordValidator, private userService: UserService, private router : Router,
              private toasterService : ToasterService) {
    this.toasterConfig = new ToasterConfig({timeout: 10000});
    this.passengerType = '';
    this.form = this.fb.group({

      firstName: ['', [
        Validators.required,
        Validators.minLength(2)
      ]],
      lastName: ['', [
        Validators.required,
        Validators.minLength(2)
      ]],
      username: ['', [
        Validators.required,
        SpaceValidator.cannotContainSpace
      ]],
      passwords: fb.group({
        password: ['', [
          Validators.required,
          Validators.minLength(5)
        ]],
        confirmPassword: ['',
          Validators.required
        ]
      }, {validator: this.passwordValidator.matchPasswords.bind(this.passwordValidator)}),
      email: ['', [
        Validators.required, Validators.email
      ]],
      address: ['',[
        Validators.required,
        Validators.minLength(5)
      ]],
      confirmation:['',[
      ]]

    })


  }

  get firstName() {
    return this.form.get('firstName');
  }

  get lastName() {
    return this.form.get('lastName');
  }

  get username() {
    return this.form.get('username');
  }

  get passwords() {
    return this.form.get('passwords');
  }

  get password() {
    return this.form.get('passwords').get('password');
  }

  get confirmPassword() {
    return this.form.get('passwords').get('confirmPassword');
  }

  get email() {
    return this.form.get('email');
  }

  get address(){
    return this.form.get('address');
  }

  get confirmation(){
    return this.form.get('confirmation');
  }
  ngOnInit() {

  }

  onchange(){
    console.log("SAF");
  }

  register(){

    console.log(this.passengerType);
    console.log(this.form.errors);
    console.log(this.firstName.value);

    let passenger = new Passenger(null, this.firstName.value, this.lastName.value, this.email.value, this.address.value,
      this.confirmation.value, this.passengerType,
      this.username.value, this.password.value);
    console.log(passenger);

    this.userService.register(passenger)
      .subscribe(() =>{
        this.router.navigate(['/login']);
      }, error => {
        console.log(error);
        this.toasterService.pop('error', 'Error', 'Username already exists!');
        if( error instanceof ConflictError  ){

          this.toasterService.pop('error', 'Error', 'Username already exists!');
        }
      })



  }



}
