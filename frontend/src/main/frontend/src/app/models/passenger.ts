export class Passenger{

  constructor(private id: number, private firstName: string, private lastName: string, private email: string,
              private address: string, private confirmation:string, private passengerType: string, private username: string,
              private password: string){}
}
