export class Employee{

  constructor(private id: number, private firstName: string, private lastName: string, private email: string,
             private address: string, private phoneNumber: number, private employmentNumber: number,
             private username: string, private password: string, private authority: string){}
}
