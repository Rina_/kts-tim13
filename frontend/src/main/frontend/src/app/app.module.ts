import { BrowserModule } from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';


import { AppComponent } from './app.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {SharedModule} from "./shared/shared.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { ToasterModule } from "angular5-toaster/dist";
import {AppRouterModule} from "./app-router.module";
import {CoreModule} from "./core/core.module";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {AppErrorHandler} from "./core/error-handlers/app-error-handler";
import {AuthService} from "./core/services/auth.service";
import {JwtService} from "./core/services/jwt.service";
import {JwtInterceptor} from "./core/interceptors/jwt-interceptor";
import { NavbarComponent } from './navbar/navbar.component';
import {LoginComponent} from "./components/login/login.component";
import {RegistrationComponent} from "./components/registration/registration.component";
import {RouterModule} from "@angular/router";
import {EmployeeService} from "./services/employee/employee.service";
import {UserService} from "./services/user/user.service";
import {NewEmployeeComponent} from "./components/new-employee/new-employee.component";
import {ProcessRequestsComponent} from "./components/process-requests/process-requests.component";
import { HomepageComponent } from './components/homepage/homepage.component';
import { AdminHomepageComponent } from './components/admin-homepage/admin-homepage.component';
import { EmployeeHomepageComponent } from './components/employee-homepage/employee-homepage.component';
import { PassengerHomepageComponent } from './components/passenger-homepage/passenger-homepage.component';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NavbarComponent,
    RegistrationComponent,
    NewEmployeeComponent,
    ProcessRequestsComponent,
    HomepageComponent,
    AdminHomepageComponent,
    EmployeeHomepageComponent,
    PassengerHomepageComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRouterModule,
    SharedModule,
    CoreModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,
    ToasterModule,
    RouterModule.forRoot([
      {
        path: 'register',
        component: RegistrationComponent
      },
      {
        path: 'new_employee',
        component: NewEmployeeComponent
      },
      {
        path: 'process_requests',
        component: ProcessRequestsComponent
      },
      {
        path: 'homepage',
        component: HomepageComponent
      },
      {
        path: 'admin_homepage',
        component: AdminHomepageComponent
      },
      {
        path: 'employee_homepage',
        component: EmployeeHomepageComponent
      },
      {
        path: 'passenger_homepage',
        component: PassengerHomepageComponent
      }
    ])
  ],
  providers: [{
    provide: ErrorHandler,
    useClass: AppErrorHandler
  }, AuthService,
    JwtService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    },
    EmployeeService, UserService]
  ,
  bootstrap: [AppComponent]
})
export class AppModule { }
