export class Token {
  username: string;
  created: Date;
  roles: Array<string>;
  id: number;
  exp: Date;
}
