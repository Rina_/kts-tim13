import { Component, OnInit } from '@angular/core';
import {JwtService} from "../core/services/jwt.service";
import {AuthService} from "../core/services/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(private jwtService : JwtService, private authService: AuthService, private router: Router) { }

  ngOnInit() {

  }


  logout(){
    this.authService.logout();
    this.router.navigate(['/homepage']);
  }

}
